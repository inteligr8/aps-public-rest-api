/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.activiti.api;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.inteligr8.activiti.model.ResultList;
import com.inteligr8.alfresco.activiti.model.DocumentTemplateLight;
import com.inteligr8.alfresco.activiti.model.EmailTemplate;
import com.inteligr8.alfresco.activiti.model.EmailTemplateLight;

/**
 * This is an undocumented API.
 * 
 * @author brian@inteligr8.com
 */
@Path("/app/rest")
public interface TemplatesApi {
    
    public enum TemplateSort {
        @JsonProperty("sort_by_name_asc")
        NameAscending,
        @JsonProperty("sort_by_name_desc")
        NameDescending
    }

    @GET
    @Path("email-templates/system")
    @Produces({ MediaType.APPLICATION_JSON })
    public ResultList<EmailTemplateLight> getSystemEmailTemplates(
            @QueryParam("tenantId") Long tenantId);

    @GET
    @Path("email-templates/custom")
    @Produces({ MediaType.APPLICATION_JSON })
    public ResultList<EmailTemplateLight> getCustomEmailTemplates(
    		@QueryParam("nameFilter") String nameFilter,
    		@QueryParam("start") Integer start,
    		@QueryParam("size") Integer size,
    		@QueryParam("sort") String sort,
    		@QueryParam("tenantId") Long tenantId);

    @GET
    @Path("email-templates/custom")
    @Produces({ MediaType.APPLICATION_JSON })
    public ResultList<EmailTemplateLight> getCustomEmailTemplates(
    		@QueryParam("nameFilter") String nameFilter,
    		@QueryParam("start") Integer start,
    		@QueryParam("size") Integer size,
    		@QueryParam("sort") TemplateSort sort,
    		@QueryParam("tenantId") Long tenantId);

    @GET
    @Path("email-templates/system/{templateName}")
    @Produces({ MediaType.APPLICATION_JSON })
    public EmailTemplate getSystemEmailTemplate(
            @PathParam("templateName") String name,
            @QueryParam("tenantId") Long tenantId);

    @GET
    @Path("email-templates/custom/{templateId}")
    @Produces({ MediaType.APPLICATION_JSON })
    public EmailTemplate getCustomEmailTemplate(
            @PathParam("templateId") long id,
            @QueryParam("tenantId") Long tenantId);

    @POST
    @Path("email-templates/custom")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public EmailTemplateLight createCustomEmailTemplate(
            EmailTemplate template);

    @PUT
    @Path("email-templates/system/{templateName}")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public EmailTemplate updateSystemEmailTemplate(
            @PathParam("templateName") String name,
            EmailTemplate template);

    @PUT
    @Path("email-templates/custom/{templateId}")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public EmailTemplate updateCustomEmailTemplate(
            @PathParam("templateId") long id,
            EmailTemplate template);

    @DELETE
    @Path("email-templates/custom/{templateId}")
    public void deleteCustomEmailTemplate(
            @PathParam("templateId") long id,
            @QueryParam("tenantId") Long tenantId);
    
    

    @GET
    @Path("document-templates")
    @Produces({ MediaType.APPLICATION_JSON })
    public ResultList<DocumentTemplateLight> getDocumentTemplates(
            @QueryParam("nameFilter") String nameFilter,
            @QueryParam("start") Integer start,
            @QueryParam("size") Integer size,
            @QueryParam("sort") String sort,
            @QueryParam("tenantId") Long tenantId);

    @GET
    @Path("document-templates")
    @Produces({ MediaType.APPLICATION_JSON })
    public ResultList<DocumentTemplateLight> getDocumentTemplates(
            @QueryParam("nameFilter") String nameFilter,
            @QueryParam("start") Integer start,
            @QueryParam("size") Integer size,
            @QueryParam("sort") TemplateSort sort,
            @QueryParam("tenantId") Long tenantId);

    @GET
    @Path("document-templates/{templateId}")
    @Produces({ MediaType.APPLICATION_JSON })
    public DocumentTemplateLight getDocumentTemplate(
            @PathParam("templateId") long id);

    @GET
    @Path("document-templates/{templateId}/file")
    @Produces({
        "application/msword",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "application/vnd.openxmlformats-officedocument.wordprocessingml.template"
    })
    public Response getDocumentTemplate(
            @PathParam("templateId") long id,
            @QueryParam("version") Long version);

    @DELETE
    @Path("document-templates/{templateId}")
    public void deleteDocumentTemplate();

}
