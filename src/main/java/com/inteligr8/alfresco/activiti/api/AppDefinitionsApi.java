/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.activiti.api;

import java.io.File;

import com.inteligr8.activiti.model.ResultList;
import com.inteligr8.alfresco.activiti.model.AppDefinitionPublishRepresentation;
import com.inteligr8.alfresco.activiti.model.AppDefinitionRepresentation;
import com.inteligr8.alfresco.activiti.model.AppDefinitionSaveRepresentation;
import com.inteligr8.alfresco.activiti.model.AppDefinitionUpdateResultRepresentation;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/api/enterprise")
public interface AppDefinitionsApi {

    @GET
    @Path("runtime-app-definitions")
    @Produces({ MediaType.APPLICATION_JSON })
    ResultList<AppDefinitionRepresentation> get();

    @GET
    @Path("app-definitions/{modelId}")
    @Produces({ MediaType.APPLICATION_JSON })
    AppDefinitionRepresentation get(
    		@PathParam("modelId") Long appId);

    @PUT
    @Path("app-definitions/{modelId}")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    AppDefinitionUpdateResultRepresentation update(
    		@PathParam("modelId") Long appId,
    		AppDefinitionSaveRepresentation updatedModel);

    @DELETE
    @Path("app-definitions/{appDefinitionId}")
    void delete(
    		@PathParam("appDefinitionId") Long appId);

    @GET
    @Path("app-definitions/{modelId}/export")
    @Produces({ MediaType.APPLICATION_JSON, "application/zip" })
    File export(
    		@PathParam("modelId") Long appId);

//    @POST
//    @Path("runtime-app-definitions")
//    @Consumes({ MediaType.APPLICATION_JSON })
//    @Produces({ MediaType.APPLICATION_JSON })
//    void publish(
//    		AppDefinitionsRepresentation publishModel);

    @POST
    @Path("app-definitions/{modelId}/publish")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    AppDefinitionUpdateResultRepresentation publish(
            @PathParam("modelId") Long appId,
            AppDefinitionPublishRepresentation publishModel);

}
