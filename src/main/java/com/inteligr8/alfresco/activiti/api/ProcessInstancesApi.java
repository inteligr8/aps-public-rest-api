/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.activiti.api;

import java.util.List;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;

import com.inteligr8.alfresco.activiti.model.CreateProcessInstance;
import com.inteligr8.alfresco.activiti.model.ProcessInstance;
import com.inteligr8.alfresco.activiti.model.ProcessInstanceFilterRepresentation;
import com.inteligr8.alfresco.activiti.model.ProcessInstanceQueryRepresentation;
import com.inteligr8.alfresco.activiti.model.ProcessInstanceVariable;
import com.inteligr8.alfresco.activiti.model.ResultListDataRepresentation;
import com.inteligr8.alfresco.activiti.model.Variable;

@Path("/api/enterprise/process-instances")
public interface ProcessInstancesApi {

    @POST
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    ProcessInstance create(CreateProcessInstance processInstance);

    @POST
    @Path("filter")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    ResultListDataRepresentation<ProcessInstance> filter(ProcessInstanceFilterRepresentation request);

    @POST
    @Path("query")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    ResultListDataRepresentation<ProcessInstance> query(ProcessInstanceQueryRepresentation request);
    
    @GET
    @Path("{processInstanceId}")
    @Produces({ "application/json" })
    ProcessInstance get(@PathParam("processInstanceId") String processInstanceId);
    
    @DELETE
    @Path("{processInstanceId}")
    void delete(@PathParam("processInstanceId") String processInstanceId);
    
    @PUT
    @Path("{processInstanceId}/activate")
    @Produces({ "application/json" })
    ProcessInstance activate(@PathParam("processInstanceId") String processInstanceId);
    
    @PUT
    @Path("{processInstanceId}/suspend")
    @Produces({ "application/json" })
    ProcessInstance suspend(@PathParam("processInstanceId") String processInstanceId);
    
    @GET
    @Path("{processInstanceId}/historic-variables")
    @Produces({ "application/json" })
    List<ProcessInstanceVariable> getHistoricVariables(@PathParam("processInstanceId") String processInstanceId);
    
    @GET
    @Path("{processInstanceId}/variables")
    @Produces({ "application/json" })
    List<Variable> getVariables(@PathParam("processInstanceId") String processInstanceId);
    
    @PUT
    @Path("{processInstanceId}/variables")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    List<Variable> setVariables(@PathParam("processInstanceId") String processInstanceId, List<Variable> variables);
    
    @GET
    @Path("{processInstanceId}/variables/{variableName}")
    @Produces({ "application/json" })
    Variable getVariable(@PathParam("processInstanceId") String processInstanceId, @PathParam("variableName") String variableName);
    
    @PUT
    @Path("{processInstanceId}/variables/{variableName}")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    Variable setVariable(@PathParam("processInstanceId") String processInstanceId, @PathParam("variableName") String variableName, Variable variable);
    
    @DELETE
    @Path("{processInstanceId}/variables/{variableName}")
    void removeVariable(@PathParam("processInstanceId") String processInstanceId, @PathParam("variableName") String variableName);

}
