
package com.inteligr8.alfresco.activiti.api;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

import com.inteligr8.alfresco.activiti.model.AppDefinitionRepresentation;
import com.inteligr8.alfresco.activiti.model.AppDefinitionUpdateResultRepresentation;
import com.inteligr8.alfresco.activiti.model.FileMultipartCxf;

@Path("/api/enterprise/app-definitions")
public interface AppDefinitionsCxfApi {

    @POST
    @Path("import")
    @Consumes({ MediaType.MULTIPART_FORM_DATA })
    @Produces({ MediaType.APPLICATION_JSON })
    AppDefinitionRepresentation import_(
    		FileMultipartCxf body,
    		@QueryParam("renewIdmEntries") Boolean renewIdmEntries);

    @POST
    @Path("{modelId}/import")
    @Consumes({ MediaType.MULTIPART_FORM_DATA })
    @Produces({ MediaType.APPLICATION_JSON })
    AppDefinitionRepresentation import_(
    		@PathParam("modelId") Long appId,
    		FileMultipartCxf body,
    		@QueryParam("renewIdmEntries") Boolean renewIdmEntries);

    @POST
    @Path("publish-app")
    @Consumes({ MediaType.MULTIPART_FORM_DATA })
    @Produces({ MediaType.APPLICATION_JSON })
    AppDefinitionUpdateResultRepresentation publishApp(
    		FileMultipartCxf body);

    @POST
    @Path("{modelId}/publish-app")
    @Consumes({ MediaType.MULTIPART_FORM_DATA })
    @Produces({ MediaType.APPLICATION_JSON })
    AppDefinitionUpdateResultRepresentation publishApp(
    		@PathParam("modelId") Long appId,
    		FileMultipartCxf body);

}
