/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.activiti.api;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

import com.inteligr8.alfresco.activiti.model.DocumentTemplateLight;
import com.inteligr8.alfresco.activiti.model.FileMultipartCxf;

/**
 * This is an undocumented API.
 * 
 * @author brian@inteligr8.com
 */
@Path("/app/rest")
public interface TemplatesCxfApi extends TemplatesApi {

    @POST
    @Path("admin/document-templates")
    @Consumes({ MediaType.MULTIPART_FORM_DATA })
    @Produces({ MediaType.APPLICATION_JSON })
    public DocumentTemplateLight createDocumentTemplate(
            @QueryParam("tenantId") Long tenantId,
            FileMultipartCxf file);

    @POST
    @Path("admin/document-templates/{templateId}")
    @Consumes({ MediaType.MULTIPART_FORM_DATA })
    @Produces({ MediaType.APPLICATION_JSON })
    public DocumentTemplateLight updateDocumentTemplate(
            @PathParam("templateId") long id,
            @QueryParam("tenantId") Long tenantId,
            FileMultipartCxf file);

}
