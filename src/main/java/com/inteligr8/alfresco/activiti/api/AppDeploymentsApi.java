/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.activiti.api;

import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

import com.inteligr8.alfresco.activiti.model.AppDeployment;
import com.inteligr8.alfresco.activiti.model.ResultListDataRepresentation;

@Path("/api/enterprise")
public interface AppDeploymentsApi {

    @GET
    @Path("runtime-app-deployment")
    @Produces({ MediaType.APPLICATION_JSON })
    AppDeployment get(
            @QueryParam("deploymentId") String deploymentId);

    @GET
    @Path("runtime-app-deployments/{deploymentId}")
    @Produces({ MediaType.APPLICATION_JSON })
    AppDeployment get(
            @PathParam("deploymentId") long deploymentId);

    @DELETE
    @Path("runtime-app-deployments/{deploymentId}")
    @Produces({ MediaType.APPLICATION_JSON })
    void remove(
            @PathParam("deploymentId") long deploymentId);

    @GET
    @Path("runtime-app-deployments")
    @Produces({ MediaType.APPLICATION_JSON })
    ResultListDataRepresentation<AppDeployment> query(
            @QueryParam("nameLike") String nameLike,
            @QueryParam("tenantId") Long tenantId,
            @QueryParam("latest") Boolean latest,
            @QueryParam("start") Integer start,
            @QueryParam("sort") String sort,
            @QueryParam("order") String order,
            @QueryParam("size") Integer size);

}
