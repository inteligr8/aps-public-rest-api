/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.activiti.api;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;

import com.inteligr8.activiti.model.ResultList;
import com.inteligr8.alfresco.activiti.model.ModelRepresentation;

@Path("/api/enterprise/models")
public interface ModelsApi {
	
	enum ModelType {
		Process(0),
		Form(2),
		App(3);
		
		private int id;
		private ModelType(int id) {
			this.id = id;
		}
		
		public int getId() {
			return this.id;
		}
	}

    @GET
	@Path("{id}")
    @Produces({ "application/json" })
    public ModelRepresentation get(
    		@PathParam("id") String id);

    @GET
    @Produces({ "application/json" })
    public ResultList<ModelRepresentation> get(
    		@QueryParam("filter") String filter,
    		@QueryParam("sort") String sort,
    		@QueryParam("modelType") Integer modelType,
    		@QueryParam("referenceId") Long referenceId);

}
