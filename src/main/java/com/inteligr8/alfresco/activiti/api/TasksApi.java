/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.activiti.api;

import java.util.List;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;

import com.inteligr8.alfresco.activiti.model.AssigneeIdentifier;
import com.inteligr8.alfresco.activiti.model.CompleteForm;
import com.inteligr8.alfresco.activiti.model.FormDefinition;
import com.inteligr8.alfresco.activiti.model.FormValue;
import com.inteligr8.alfresco.activiti.model.ResultListDataRepresentation;
import com.inteligr8.alfresco.activiti.model.Task;
import com.inteligr8.alfresco.activiti.model.TaskFilterRepresentation;
import com.inteligr8.alfresco.activiti.model.TaskQueryRepresentation;
import com.inteligr8.alfresco.activiti.model.TaskUpdate;
import com.inteligr8.alfresco.activiti.model.UserIdentifier;
import com.inteligr8.alfresco.activiti.model.Variable;

@Path("/api/enterprise")
public interface TasksApi {

    @GET
    @Path("tasks/{taskId}")
    @Produces({ "application/json" })
    Task get(@PathParam("taskId") String taskId);

    @GET
    @Path("task-forms/{taskId}")
    @Produces({ "application/json" })
    FormDefinition getForm(@PathParam("taskId") String taskId);

    @GET
    @Path("task-forms/{taskId}/form-values/{field}")
    @Produces({ "application/json" })
    FormValue getFormValue(@PathParam("taskId") String taskId, @PathParam("field") String field);

    @GET
    @Path("task-forms/{taskId}/form-values/{field}/{column}")
    @Produces({ "application/json" })
    FormValue getFormValue(@PathParam("taskId") String taskId, @PathParam("field") String field, @PathParam("column") String column);

    @PUT
    @Path("tasks/{taskId}")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    Task update(@PathParam("taskId") String taskId, TaskUpdate taskUpdate);

    @DELETE
    @Path("tasks/{taskId}")
    @Produces({ "application/json" })
    void delete(@PathParam("taskId") String taskId);

    @POST
    @Path("tasks/query")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    ResultListDataRepresentation<Task> query(TaskQueryRepresentation request);

    @POST
    @Path("tasks/filter")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    ResultListDataRepresentation<Task> filter(TaskFilterRepresentation request);

    @PUT
    @Path("tasks/{taskId}/action/assign")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    Task assign(@PathParam("taskId") String taskId, AssigneeIdentifier request);

    @PUT
    @Path("tasks/{taskId}/action/claim")
    void claim(@PathParam("taskId") String taskId);

    @PUT
    @Path("tasks/{taskId}/action/complete")
    void complete(@PathParam("taskId") String taskId);

    @POST
    @Path("task-forms/{taskId}")
    @Consumes({ "application/json" })
    void complete(@PathParam("taskId") String taskId, CompleteForm request);

    @PUT
    @Path("tasks/{taskId}/action/delegate")
    @Consumes({ "application/json" })
    void delegate(@PathParam("taskId") String taskId, UserIdentifier request);

    @PUT
    @Path("tasks/{taskId}/action/involve")
    @Consumes({ "application/json" })
    void involve(@PathParam("taskId") String taskId, UserIdentifier request);

    @POST
    @Path("tasks/{taskId}/groups/{groupId}")
    void involveGroup(@PathParam("taskId") String taskId, @PathParam("groupId") String groupId);

    @DELETE
    @Path("tasks/{taskId}/groups/{groupId}")
    void removeInvolvedGroup(@PathParam("taskId") String taskId, @PathParam("groupId") String groupId);

    @PUT
    @Path("tasks/{taskId}/action/remove-involved")
    @Consumes({ "application/json" })
    void removeInvolved(@PathParam("taskId") String taskId, UserIdentifier request);

    @PUT
    @Path("tasks/{taskId}/action/resolve")
    void resolve(@PathParam("taskId") String taskId);

    @PUT
    @Path("tasks/{taskId}/action/unclaim")
    void unclaim(@PathParam("taskId") String taskId);
    
    @GET
    @Path("tasks/{taskId}/variables")
    @Produces({ "application/json" })
    List<Variable> getVariables(@PathParam("taskId") String taskId, @QueryParam("scope") String scope);
    
    @POST
    @Path("tasks/{taskId}/variables")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    List<Variable> setVariables(@PathParam("taskId") String taskId, List<Variable> variables);
    
    @DELETE
    @Path("tasks/{taskId}/variables")
    List<Variable> removeVariables(@PathParam("taskId") String taskId);
    
    @GET
    @Path("tasks/{taskId}/variables/{variableName}")
    @Produces({ "application/json" })
    Variable getVariable(@PathParam("taskId") String taskId, @PathParam("variableName") String variableName, @QueryParam("scope") String scope);
    
    @PUT
    @Path("tasks/{taskId}/variables/{variableName}")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    Variable setVariable(@PathParam("taskId") String taskId, @PathParam("variableName") String variableName, Variable variable);
    
    @DELETE
    @Path("tasks/{taskId}/variables/{variableName}")
    void removeVariable(@PathParam("taskId") String taskId, @PathParam("variableName") String variableName, @QueryParam("scope") String scope);

}
