/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.activiti.api;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;

import com.inteligr8.activiti.model.ResultList;
import com.inteligr8.alfresco.activiti.model.GroupLight;
import com.inteligr8.alfresco.activiti.model.UserLight;

@Path("/api/enterprise/groups")
public interface GroupsApi {

    @GET
    @Produces({ "application/json" })
    ResultList<GroupLight> query(
    		@QueryParam("filter") String filter,
    		@QueryParam("groupId") Long groupId,
    		@QueryParam("externalId") String externalId,
    		@QueryParam("externalIdCaseInsensitive") String externalIdCaseInsensitve,
    		@QueryParam("tenantId") Long tenantId);

    @GET
    @Path("{groupId}/users")
    @Produces({ "application/json" })
    ResultList<UserLight> queryMembers(
    		@PathParam("groupId") long groupId);

}
