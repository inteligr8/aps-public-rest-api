/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.activiti.api;

import java.util.List;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

import com.inteligr8.activiti.model.ResultList;
import com.inteligr8.alfresco.activiti.model.Group;
import com.inteligr8.alfresco.activiti.model.GroupLight;
import com.inteligr8.alfresco.activiti.model.Tenant;
import com.inteligr8.alfresco.activiti.model.UserLight;

@Path("/api/enterprise/admin")
public interface AdminApi {

    @GET
    @Path("/tenants")
    @Produces({ MediaType.APPLICATION_JSON })
    public List<Tenant> getTenants();
    
    @GET
    @Path("/groups")
    @Produces({ MediaType.APPLICATION_JSON })
    public List<GroupLight> getGroups(
    		@QueryParam("tenantId") Long tenantId,
    		@QueryParam("functional") Boolean functional,
    		@QueryParam("summary") Boolean summary);
    
    @POST
    @Path("/groups")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public GroupLight createGroup(
    		Group group);
    
    @GET
    @Path("/groups/{groupId}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Group getGroup(
    		@PathParam("groupId") long groupId,
    		@QueryParam("includeAllUsers") Boolean includeAllUsers,
    		@QueryParam("summary") Boolean summary);
    
    @GET
    @Path("/groups/${groupId}/related-groups")
    @Produces({ MediaType.APPLICATION_JSON })
    public List<GroupLight> getRelatedGroups(
    		@PathParam("groupId") long groupId);
    
    @POST
    @Path("/groups/${groupId}/related-groups/${relatedGroupId}")
    @Produces({ MediaType.APPLICATION_JSON })
    public void addRelatedGroup(
    		@PathParam("groupId") long groupId,
    		@PathParam("relatedGroupId") long relatedGroupId,
    		@PathParam("type") String type);
    
    @DELETE
    @Path("/groups/${groupId}/related-groups/${relatedGroupId}")
    @Produces({ MediaType.APPLICATION_JSON })
    public void deleteRelatedGroup(
    		@PathParam("groupId") long groupId,
    		@PathParam("relatedGroupId") long relatedGroupId);

    @GET
    @Path("groups/{groupId}/users")
    @Produces({ MediaType.APPLICATION_JSON })
    ResultList<UserLight> getMembers(
    		@PathParam("groupId") long groupId,
    		@QueryParam("filter") String filter,
    		@QueryParam("page") Integer page,
    		@QueryParam("pageSize") Integer pageSize);

}
