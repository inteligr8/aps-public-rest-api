/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.activiti.api;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;

import com.inteligr8.alfresco.activiti.model.HistoricProcessInstanceQueryRepresentation;
import com.inteligr8.alfresco.activiti.model.HistoricTaskQueryRepresentation;
import com.inteligr8.alfresco.activiti.model.ProcessInstance;
import com.inteligr8.alfresco.activiti.model.ResultListDataRepresentation;
import com.inteligr8.alfresco.activiti.model.Task;

@Path("/api/enterprise")
public interface HistoricApi {

    @POST
    @Path("historic-process-instances/query")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    ResultListDataRepresentation<ProcessInstance> query(HistoricProcessInstanceQueryRepresentation request);

    @POST
    @Path("historic-tasks/query")
    @Consumes({ "application/json" })
    @Produces({ "application/json" })
    ResultListDataRepresentation<Task> query(HistoricTaskQueryRepresentation request);

}
