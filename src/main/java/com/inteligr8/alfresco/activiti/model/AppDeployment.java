
package com.inteligr8.alfresco.activiti.model;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.inteligr8.activiti.model.Datum;


/**
 * AppDeploymentRepresentation
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "appDefinition",
    "created",
    "createdBy",
    "deploymentId",
    "dmnDeploymentId"
})
public class AppDeployment extends Datum {

    @JsonProperty("id")
    private long id;
    @JsonProperty("appDefinition")
    private AppDefinition appDefinition;
    @JsonProperty("created")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime created;
    @JsonProperty("createdBy")
    private UserLight createdBy;
    @JsonProperty("deploymentId")
    private String deploymentId;
    @JsonProperty("dmnDeploymentId")
    private Long dmnDeploymentId;
    
    public long getId() {
        return id;
    }
    
    public void setId(long id) {
        this.id = id;
    }
    
    public AppDeployment withId(long id) {
        this.id = id;
        return this;
    }
    
    public AppDefinition getAppDefinition() {
        return appDefinition;
    }
    
    public void setAppDefinition(AppDefinition appDefinition) {
        this.appDefinition = appDefinition;
    }
    
    public AppDeployment withAppDefinition(AppDefinition appDefinition) {
        this.appDefinition = appDefinition;
        return this;
    }
    
    public OffsetDateTime getCreated() {
        return created;
    }
    
    public void setCreated(OffsetDateTime created) {
        this.created = created;
    }
    
    public AppDeployment withCreated(OffsetDateTime created) {
        this.created = created;
        return this;
    }
    
    public UserLight getCreatedBy() {
        return createdBy;
    }
    
    public void setCreatedBy(UserLight createdBy) {
        this.createdBy = createdBy;
    }
    
    public AppDeployment withCreatedBy(UserLight createdBy) {
        this.createdBy = createdBy;
        return this;
    }
    
    public String getDeploymentId() {
        return deploymentId;
    }
    
    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }
    
    public AppDeployment withDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
        return this;
    }
    
    public Long getDmnDeploymentId() {
        return dmnDeploymentId;
    }
    
    public void setDmnDeploymentId(Long dmnDeploymentId) {
        this.dmnDeploymentId = dmnDeploymentId;
    }
    
    public AppDeployment withDmnDeploymentId(Long dmnDeploymentId) {
        this.dmnDeploymentId = dmnDeploymentId;
        return this;
    }

}
