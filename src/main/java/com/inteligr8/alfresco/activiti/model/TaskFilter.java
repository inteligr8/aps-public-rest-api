

package com.inteligr8.alfresco.activiti.model;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.inteligr8.activiti.model.Datum;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "assignment",
    "dueAfter",
    "dueBefore",
    "processDefinitionId",
    "processDefinitionKey",
    "sort",
    "asc",
    "state"
})
public class TaskFilter extends Datum {
    
    public enum State {
        @JsonProperty("all")
        All,
        @JsonProperty("completed")
        Completed,
        @JsonProperty("running")
        Running
    }

    @JsonProperty("name")
    private String name;
    @JsonProperty("assignment")
    private String assignment;
    @JsonProperty("dueBefore")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime dueBefore;
    @JsonProperty("dueAfter")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime dueAfter;
    @JsonProperty("processDefinitionId")
    private String processDefinitionId;
    @JsonProperty("processDefinitionKey")
    private String processDefinitionKey;
    @JsonProperty("sort")
    private String sort;
    @JsonProperty("asc")
    private Boolean asc;
    @JsonProperty("state")
    private State state;

    /**
     * No args constructor for use in serialization
     */
    public TaskFilter() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TaskFilter withName(String name) {
        this.name = name;
        return this;
    }
    
    public String getAssignment() {
        return assignment;
    }
    
    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }

    public TaskFilter withAssignment(String assignment) {
        this.assignment = assignment;
        return this;
    }
    
    public OffsetDateTime getDueAfter() {
        return dueAfter;
    }
    
    public void setDueAfter(OffsetDateTime dueAfter) {
        this.dueAfter = dueAfter;
    }
    
    public TaskFilter withDueAfter(OffsetDateTime dueAfter) {
        this.dueAfter = dueAfter;
        return this;
    }
    
    public OffsetDateTime getDueBefore() {
        return dueBefore;
    }
    
    public void setDueBefore(OffsetDateTime dueBefore) {
        this.dueBefore = dueBefore;
    }
    
    public TaskFilter withDueBefore(OffsetDateTime dueBefore) {
        this.dueBefore = dueBefore;
        return this;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String id) {
        this.processDefinitionId = id;
    }

    public TaskFilter withProcessDefinitionId(String id) {
        this.processDefinitionId = id;
        return this;
    }
    
    public String getProcessDefinitionKey() {
        return processDefinitionKey;
    }
    
    public void setProcessDefinitionKey(String key) {
        this.processDefinitionKey = key;
    }
    
    public TaskFilter withProcessDefinitionKey(String key) {
        this.processDefinitionKey = key;
        return this;
    }
    
    public String getSort() {
        return sort;
    }
    
    public void setSort(String sort) {
        this.sort = sort;
    }
    
    public TaskFilter withSort(String sort) {
        this.sort = sort;
        return this;
    }
    
    public Boolean isAsc() {
        return asc;
    }
    
    public void setAsc(Boolean asc) {
        this.asc = asc;
    }
    
    public TaskFilter withAsc(Boolean asc) {
        this.asc = asc;
        return this;
    }
    
    public State getState() {
        return state;
    }
    
    public void setState(State state) {
        this.state = state;
    }
    
    public TaskFilter withState(State state) {
        this.state = state;
        return this;
    }

}
