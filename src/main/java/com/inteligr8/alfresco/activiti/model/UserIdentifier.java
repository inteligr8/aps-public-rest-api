package com.inteligr8.alfresco.activiti.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserIdentifier {
	
	@JsonProperty
	private String userId;
	@JsonProperty
	private String email;
	
    public String getUserId() {
        return userId;
    }
    
    public void setUserId(String userId) {
        this.userId = userId;
    }

	public UserIdentifier withUserId(String userId) {
        this.userId = userId;
        return this;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }

	public UserIdentifier withEmail(String email) {
        this.email = email;
        return this;
    }

}
