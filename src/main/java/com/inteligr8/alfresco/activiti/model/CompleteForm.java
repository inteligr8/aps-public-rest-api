package com.inteligr8.alfresco.activiti.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "outcome"
})
public class CompleteForm {

    @JsonProperty("outcome")
    private String outcome;

    /**
     * No args constructor for use in serialization
     */
    public CompleteForm() {
    }

    public CompleteForm(String outcome) {
        this.outcome = outcome;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }
    
    public CompleteForm withOutcome(String outcome) {
        this.outcome = outcome;
        return this;
    }
    
}
