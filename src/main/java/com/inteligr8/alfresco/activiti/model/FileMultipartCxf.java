package com.inteligr8.alfresco.activiti.model;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.ContentDisposition;
import org.apache.cxf.jaxrs.ext.multipart.MultipartBody;

public class FileMultipartCxf extends MultipartBody {
	
	public static FileMultipartCxf from(String filename, InputStream istream) throws IOException {
		return new FileMultipartCxf(Arrays.asList(toAttachment(filename, istream)));
	}
 	
	public FileMultipartCxf(List<Attachment> atts) throws IOException {
		super(atts);
	}
 	
	public FileMultipartCxf(List<Attachment> atts, boolean outbound) {
		super(atts, outbound);
	}
	
	public Attachment getFileAttachment() {
		return this.getAttachment("file");
	}

	private static Attachment toAttachment(String filename, InputStream istream) {
		if (filename == null) {
			return new Attachment("file", istream, new ContentDisposition("form-data; name=\"file\""));
		} else {
			return new Attachment("file", istream, new ContentDisposition("form-data; name=\"file\"; filename=\"" + filename + "\""));
		}
	}

}
