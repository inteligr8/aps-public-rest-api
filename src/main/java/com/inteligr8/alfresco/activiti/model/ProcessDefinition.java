

package com.inteligr8.alfresco.activiti.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.inteligr8.activiti.model.Datum;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "key",
    "name",
    "description",
    "hasStartForm",
    "deploymentId",
    "version",
    "category",
    "tenantId"
})
public class ProcessDefinition extends Datum {

    @JsonProperty("category")
    private String category;
    @JsonProperty("deploymentId")
    private String deploymentId;
    @JsonProperty("description")
    private String description;
    @JsonProperty("hasStartForm")
    private boolean hasStartForm;
    @JsonProperty("id")
    private String id;
    @JsonProperty("key")
    private String key;
    @JsonProperty("name")
    private String name;
    @JsonProperty("tenantId")
    private String tenantId;
    @JsonProperty("version")
    private Integer version;

    /**
     * No args constructor for use in serialization
     */
    public ProcessDefinition() {
    }
    
    public String getCategory() {
        return category;
    }
    
    public void setCategory(String category) {
        this.category = category;
    }
    
    public ProcessDefinition withCategory(String category) {
        this.category = category;
        return this;
    }
    
    public String getDeploymentId() {
        return deploymentId;
    }
    
    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }
    
    public ProcessDefinition withDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
        return this;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public ProcessDefinition withDescription(String description) {
        this.description = description;
        return this;
    }
    
    public boolean isHasStartForm() {
        return hasStartForm;
    }
    
    public void setHasStartForm(boolean hasStartForm) {
        this.hasStartForm = hasStartForm;
    }
    
    public ProcessDefinition withHasStartForm(boolean hasStartForm) {
        this.hasStartForm = hasStartForm;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProcessDefinition withId(String id) {
        this.id = id;
        return this;
    }
    
    public String getKey() {
        return key;
    }
    
    public void setKey(String key) {
        this.key = key;
    }
    
    public ProcessDefinition withKey(String key) {
        this.key = key;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProcessDefinition withName(String name) {
        this.name = name;
        return this;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public ProcessDefinition withTenantId(String tenantId) {
        this.tenantId = tenantId;
        return this;
    }
    
    public Integer getVersion() {
        return version;
    }
    
    public void setVersion(Integer version) {
        this.version = version;
    }
    
    public ProcessDefinition withVersion(Integer version) {
        this.version = version;
        return this;
    }

}
