
package com.inteligr8.alfresco.activiti.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.inteligr8.activiti.model.Datum;


/**
 * ResultListDataRepresentation
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "data",
    "size",
    "start",
    "total"
})
public class ResultListDataRepresentation<T extends Datum> extends Datum {

    @JsonProperty("data")
    private List<T> data = new ArrayList<>();
    @JsonProperty("size")
    private Integer size;
    @JsonProperty("start")
    private Integer start;
    @JsonProperty("total")
    private Integer total;

    @JsonProperty("data")
    public List<T> getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(List<T> data) {
        this.data = data;
    }

    public ResultListDataRepresentation<T> withData(List<T> data) {
        this.data = data;
        return this;
    }

    @JsonProperty("size")
    public Integer getSize() {
        return size;
    }

    @JsonProperty("size")
    public void setSize(Integer size) {
        this.size = size;
    }

    public ResultListDataRepresentation<T> withSize(Integer size) {
        this.size = size;
        return this;
    }

    @JsonProperty("start")
    public Integer getStart() {
        return start;
    }

    @JsonProperty("start")
    public void setStart(Integer start) {
        this.start = start;
    }

    public ResultListDataRepresentation<T> withStart(Integer start) {
        this.start = start;
        return this;
    }

    @JsonProperty("total")
    public Integer getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(Integer total) {
        this.total = total;
    }

    public ResultListDataRepresentation<T> withTotal(Integer total) {
        this.total = total;
        return this;
    }

}
