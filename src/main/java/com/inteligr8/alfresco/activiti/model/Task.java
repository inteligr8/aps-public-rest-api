
package com.inteligr8.alfresco.activiti.model;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.inteligr8.activiti.model.Datum;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "adhocTaskCanBeReassigned",
    "assignee",
    "category",
    "created",
    "description",
    "dueDate",
    "duration",
    "endDate",
    "executionId",
    "formKey",
    "id",
    "initiatorCanCompleteTask",
    "involvedPeople",
    "managerOfCandidateGroup",
    "memberOfCandidateGroup",
    "memberOfCandidateUsers",
    "name",
    "parentTaskId",
    "parentTaskName",
    "priority",
    "processDefinitionCategory",
    "processDefinitionDeploymentId",
    "processDefinitionDescription",
    "processDefinitionId",
    "processDefinitionKey",
    "processDefinitionName",
    "processDefinitionVersion",
    "processInstanceId",
    "processInstanceName",
    "processInstanceStartUserId",
    "taskDefinitionKey",
    "variables"
})
public class Task extends Datum {

    @JsonProperty("adhocTaskCanBeReassigned")
    private Boolean adhocTaskCanBeReassigned;
    @JsonProperty("assignee")
    private UserLight assignee;
    @JsonProperty("category")
    private String category;
    @JsonProperty("created")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime created;
    @JsonProperty("description")
    private String description;
    @JsonProperty("dueDate")
    private OffsetDateTime dueDate;
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    @JsonProperty("duration")
    private Long duration;
    @JsonProperty("endDate")
    private OffsetDateTime endDate;
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    @JsonProperty("executionId")
    private String executionId;
    @JsonProperty("formKey")
    private String formKey;
    @JsonProperty("id")
    private String id;
    @JsonProperty("initiatorCanCompleteTask")
    private Boolean initiatorCanCompleteTask;
    @JsonProperty("involvedPeople")
    private List<UserLight> involvedPeople = new ArrayList<UserLight>();
    @JsonProperty("managerOfCandidateGroup")
    private Boolean managerOfCandidateGroup;
    @JsonProperty("memberOfCandidateGroup")
    private Boolean memberOfCandidateGroup;
    @JsonProperty("memberOfCandidateUsers")
    private Boolean memberOfCandidateUsers;
    @JsonProperty("name")
    private String name;
    @JsonProperty("parentTaskId")
    private String parentTaskId;
    @JsonProperty("parentTaskName")
    private String parentTaskName;
    @JsonProperty("priority")
    private Integer priority;
    @JsonProperty("processDefinitionCategory")
    private String processDefinitionCategory;
    @JsonProperty("processDefinitionDeploymentId")
    private String processDefinitionDeploymentId;
    @JsonProperty("processDefinitionDescription")
    private String processDefinitionDescription;
    @JsonProperty("processDefinitionId")
    private String processDefinitionId;
    @JsonProperty("processDefinitionKey")
    private String processDefinitionKey;
    @JsonProperty("processDefinitionName")
    private String processDefinitionName;
    @JsonProperty("processDefinitionVersion")
    private Integer processDefinitionVersion;
    @JsonProperty("processInstanceId")
    private String processInstanceId;
    @JsonProperty("processInstanceName")
    private String processInstanceName;
    @JsonProperty("processInstanceStartUserId")
    private String processInstanceStartUserId;
    @JsonProperty("taskDefinitionKey")
    private String taskDefinitionKey;
    @JsonProperty("variables")
    private List<Variable> variables = new ArrayList<Variable>();

    /**
     * No args constructor for use in serialization
     */
    public Task() {
    }

    public Boolean getAdhocTaskCanBeReassigned() {
        return adhocTaskCanBeReassigned;
    }

    public void setAdhocTaskCanBeReassigned(Boolean adhocTaskCanBeReassigned) {
        this.adhocTaskCanBeReassigned = adhocTaskCanBeReassigned;
    }

    public Task withAdhocTaskCanBeReassigned(Boolean adhocTaskCanBeReassigned) {
        this.adhocTaskCanBeReassigned = adhocTaskCanBeReassigned;
        return this;
    }

    public UserLight getAssignee() {
        return assignee;
    }
    
    public void setAssignee(UserLight assignee) {
        this.assignee = assignee;
    }

    public Task withAssignee(UserLight assignee) {
        this.assignee = assignee;
        return this;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Task withCategory(String category) {
        this.category = category;
        return this;
    }

    public OffsetDateTime getCreated() {
        return created;
    }

    public void setCreated(OffsetDateTime created) {
        this.created = created;
    }

    public Task withCreated(OffsetDateTime created) {
        this.created = created;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Task withDescription(String description) {
        this.description = description;
        return this;
    }

    public OffsetDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(OffsetDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public Task withDueDate(OffsetDateTime dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Task withDuration(Long duration) {
        this.duration = duration;
        return this;
    }

    public OffsetDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(OffsetDateTime endDate) {
        this.endDate = endDate;
    }

    public Task withEndDate(OffsetDateTime endDate) {
        this.endDate = endDate;
        return this;
    }

    public String getExecutionId() {
        return executionId;
    }

    public void setExecutionId(String executionId) {
        this.executionId = executionId;
    }

    public Task withExecutionId(String executionId) {
        this.executionId = executionId;
        return this;
    }

    public String getFormKey() {
        return formKey;
    }

    public void setFormKey(String formKey) {
        this.formKey = formKey;
    }

    public Task withFormKey(String formKey) {
        this.formKey = formKey;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Task withId(String id) {
        this.id = id;
        return this;
    }

    public Boolean getInitiatorCanCompleteTask() {
        return initiatorCanCompleteTask;
    }

    public void setInitiatorCanCompleteTask(Boolean initiatorCanCompleteTask) {
        this.initiatorCanCompleteTask = initiatorCanCompleteTask;
    }

    public Task withInitiatorCanCompleteTask(Boolean initiatorCanCompleteTask) {
        this.initiatorCanCompleteTask = initiatorCanCompleteTask;
        return this;
    }

    public List<UserLight> getInvolvedPeople() {
        return involvedPeople;
    }

    public void setInvolvedPeople(List<UserLight> involvedPeople) {
        this.involvedPeople = involvedPeople;
    }

    public Task withInvolvedPeople(List<UserLight> involvedPeople) {
        this.involvedPeople = involvedPeople;
        return this;
    }

    public Boolean getManagerOfCandidateGroup() {
        return managerOfCandidateGroup;
    }

    public void setManagerOfCandidateGroup(Boolean managerOfCandidateGroup) {
        this.managerOfCandidateGroup = managerOfCandidateGroup;
    }

    public Task withManagerOfCandidateGroup(Boolean managerOfCandidateGroup) {
        this.managerOfCandidateGroup = managerOfCandidateGroup;
        return this;
    }

    public Boolean getMemberOfCandidateGroup() {
        return memberOfCandidateGroup;
    }

    public void setMemberOfCandidateGroup(Boolean memberOfCandidateGroup) {
        this.memberOfCandidateGroup = memberOfCandidateGroup;
    }

    public Task withMemberOfCandidateGroup(Boolean memberOfCandidateGroup) {
        this.memberOfCandidateGroup = memberOfCandidateGroup;
        return this;
    }

    public Boolean getMemberOfCandidateUsers() {
        return memberOfCandidateUsers;
    }

    public void setMemberOfCandidateUsers(Boolean memberOfCandidateUsers) {
        this.memberOfCandidateUsers = memberOfCandidateUsers;
    }

    public Task withMemberOfCandidateUsers(Boolean memberOfCandidateUsers) {
        this.memberOfCandidateUsers = memberOfCandidateUsers;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Task withName(String name) {
        this.name = name;
        return this;
    }

    public String getParentTaskId() {
        return parentTaskId;
    }

    public void setParentTaskId(String parentTaskId) {
        this.parentTaskId = parentTaskId;
    }

    public Task withParentTaskId(String parentTaskId) {
        this.parentTaskId = parentTaskId;
        return this;
    }

    public String getParentTaskName() {
        return parentTaskName;
    }

    public void setParentTaskName(String parentTaskName) {
        this.parentTaskName = parentTaskName;
    }

    public Task withParentTaskName(String parentTaskName) {
        this.parentTaskName = parentTaskName;
        return this;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Task withPriority(Integer priority) {
        this.priority = priority;
        return this;
    }

    public String getProcessDefinitionCategory() {
        return processDefinitionCategory;
    }

    public void setProcessDefinitionCategory(String processDefinitionCategory) {
        this.processDefinitionCategory = processDefinitionCategory;
    }

    public Task withProcessDefinitionCategory(String processDefinitionCategory) {
        this.processDefinitionCategory = processDefinitionCategory;
        return this;
    }

    public String getProcessDefinitionDeploymentId() {
        return processDefinitionDeploymentId;
    }

    public void setProcessDefinitionDeploymentId(String processDefinitionDeploymentId) {
        this.processDefinitionDeploymentId = processDefinitionDeploymentId;
    }

    public Task withProcessDefinitionDeploymentId(String processDefinitionDeploymentId) {
        this.processDefinitionDeploymentId = processDefinitionDeploymentId;
        return this;
    }

    public String getProcessDefinitionDescription() {
        return processDefinitionDescription;
    }

    public void setProcessDefinitionDescription(String processDefinitionDescription) {
        this.processDefinitionDescription = processDefinitionDescription;
    }

    public Task withProcessDefinitionDescription(String processDefinitionDescription) {
        this.processDefinitionDescription = processDefinitionDescription;
        return this;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public Task withProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
        return this;
    }

    public String getProcessDefinitionKey() {
        return processDefinitionKey;
    }

    public void setProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
    }

    public Task withProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
        return this;
    }

    public String getProcessDefinitionName() {
        return processDefinitionName;
    }

    public void setProcessDefinitionName(String processDefinitionName) {
        this.processDefinitionName = processDefinitionName;
    }

    public Task withProcessDefinitionName(String processDefinitionName) {
        this.processDefinitionName = processDefinitionName;
        return this;
    }

    public Integer getProcessDefinitionVersion() {
        return processDefinitionVersion;
    }

    public void setProcessDefinitionVersion(Integer processDefinitionVersion) {
        this.processDefinitionVersion = processDefinitionVersion;
    }

    public Task withProcessDefinitionVersion(Integer processDefinitionVersion) {
        this.processDefinitionVersion = processDefinitionVersion;
        return this;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public Task withProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
        return this;
    }

    public String getProcessInstanceName() {
        return processInstanceName;
    }

    public void setProcessInstanceName(String processInstanceName) {
        this.processInstanceName = processInstanceName;
    }

    public Task withProcessInstanceName(String processInstanceName) {
        this.processInstanceName = processInstanceName;
        return this;
    }

    public String getProcessInstanceStartUserId() {
        return processInstanceStartUserId;
    }

    public void setProcessInstanceStartUserId(String processInstanceStartUserId) {
        this.processInstanceStartUserId = processInstanceStartUserId;
    }

    public Task withProcessInstanceStartUserId(String processInstanceStartUserId) {
        this.processInstanceStartUserId = processInstanceStartUserId;
        return this;
    }

    public String getTaskDefinitionKey() {
        return taskDefinitionKey;
    }

    public void setTaskDefinitionKey(String taskDefinitionKey) {
        this.taskDefinitionKey = taskDefinitionKey;
    }

    public Task withTaskDefinitionKey(String taskDefinitionKey) {
        this.taskDefinitionKey = taskDefinitionKey;
        return this;
    }

    public List<Variable> getVariables() {
        return variables;
    }

    public void setVariables(List<Variable> variables) {
        this.variables = variables;
    }

    public Task withVariables(List<Variable> variables) {
        this.variables = variables;
        return this;
    }

}
