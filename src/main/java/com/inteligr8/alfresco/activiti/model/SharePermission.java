package com.inteligr8.alfresco.activiti.model;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SharePermission extends Permission {
	
	@JsonProperty
	private OffsetDateTime shareDate;
	@JsonProperty
	private Long sharedBy;
	
	public OffsetDateTime getShareDate() {
		return shareDate;
	}
	
	public void setShareDate(OffsetDateTime shareDate) {
		this.shareDate = shareDate;
	}
	
	public Long getSharedBy() {
		return sharedBy;
	}
	
	public void setSharedBy(Long sharedBy) {
		this.sharedBy = sharedBy;
	}

}
