package com.inteligr8.alfresco.activiti.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "capabilities",
    "externalId",
    "groups",
    "id",
    "lastSyncTimeStamp",
    "manager",
    "name",
    "parentGroupId",
    "status",
    "tenantId",
    "type",
    "userCount",
    "users"
})
public class Group {

    @JsonProperty("capabilities")
    private List<GroupCapability> capabilities = new ArrayList<GroupCapability>();
    @JsonProperty("externalId")
    private String externalId;
    @JsonProperty("groups")
    private List<GroupLight> groups = new ArrayList<GroupLight>();
    @JsonProperty("id")
    private Long id;
    @JsonProperty("lastSyncTimeStamp")
    private String lastSyncTimeStamp;
    @JsonProperty("manager")
    private User manager;
    @JsonProperty("name")
    private String name;
    @JsonProperty("parentGroupId")
    private Long parentGroupId;
    @JsonProperty("status")
    private String status;
    @JsonProperty("tenantId")
    private Long tenantId;
    @JsonProperty("type")
    private Long type;
    @JsonProperty("userCount")
    private Long userCount;
    @JsonProperty("users")
    private List<User> users;

    /**
     * No args constructor for use in serialization
     */
    public Group() {
    }

    public List<GroupCapability> getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(List<GroupCapability> capabilities) {
        this.capabilities = capabilities;
    }

    public Group withCapabilities(List<GroupCapability> capabilities) {
        this.capabilities = capabilities;
        return this;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public Group withExternalId(String externalId) {
        this.externalId = externalId;
        return this;
    }

    public List<GroupLight> getGroups() {
        return groups;
    }

    public void setGroups(List<GroupLight> groups) {
        this.groups = groups;
    }

    public Group withGroups(List<GroupLight> groups) {
        this.groups = groups;
        return this;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Group withId(Long id) {
        this.id = id;
        return this;
    }

    public String getLastSyncTimeStamp() {
        return lastSyncTimeStamp;
    }

    public void setLastSyncTimeStamp(String lastSyncTimeStamp) {
        this.lastSyncTimeStamp = lastSyncTimeStamp;
    }

    public Group withLastSyncTimeStamp(String lastSyncTimeStamp) {
        this.lastSyncTimeStamp = lastSyncTimeStamp;
        return this;
    }

    public User getManager() {
        return manager;
    }

    public void setManager(User manager) {
        this.manager = manager;
    }

    public Group withManager(User manager) {
        this.manager = manager;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Group withName(String name) {
        this.name = name;
        return this;
    }

    public Long getParentGroupId() {
        return parentGroupId;
    }

    public void setParentGroupId(Long parentGroupId) {
        this.parentGroupId = parentGroupId;
    }

    public Group withParentGroupId(Long parentGroupId) {
        this.parentGroupId = parentGroupId;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Group withStatus(String status) {
        this.status = status;
        return this;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public Group withTenantId(Long tenantId) {
        this.tenantId = tenantId;
        return this;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    public Group withType(Long type) {
        this.type = type;
        return this;
    }

    public Long getUserCount() {
        return userCount;
    }

    public void setUserCount(Long userCount) {
        this.userCount = userCount;
    }

    public Group withUserCount(Long userCount) {
        this.userCount = userCount;
        return this;
    }
    
    public List<User> getUsers() {
		return users;
	}
    
    public void setUsers(List<User> users) {
		this.users = users;
	}
    
    public Group withUsers(List<User> users) {
		this.setUsers(users);
		return this;
	}

}




