

package com.inteligr8.alfresco.activiti.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "appDefinitionId",
    "page",
    "processDefinitionId",
    "processInstanceId",
    "size",
    "sort",
    "start",
    "state"
})
public class ProcessInstanceQueryRepresentation {
    
    public enum Sort {
        @JsonProperty("created-asc")
        CreatedAsc,
        @JsonProperty("created-desc")
        CreatedDesc,
        @JsonProperty("ended-asc")
        EndedAsc,
        @JsonProperty("ended-desc")
        EndedDesc,
    }
    
    public enum State {
        @JsonProperty("all")
        All,
        @JsonProperty("completed")
        Completed,
        @JsonProperty("running")
        Running
    }

    @JsonProperty("appDefinitionId")
    private Long appDefinitionId;
    @JsonProperty("processDefinitionId")
    private String processDefinitionId;
    @JsonProperty("processInstanceId")
    private String processInstanceId;
    @JsonProperty("page")
    private Integer page;
    @JsonProperty("size")
    private Integer size;
    @JsonProperty("sort")
    private Sort sort;
    @JsonProperty("start")
    private Integer start;
    @JsonProperty("state")
    private State state;

    /**
     * No args constructor for use in serialization
     */
    public ProcessInstanceQueryRepresentation() {
    }

    public Long getAppDefinitionId() {
        return appDefinitionId;
    }

    public void setAppDefinitionId(Long appDefinitionId) {
        this.appDefinitionId = appDefinitionId;
    }

    public ProcessInstanceQueryRepresentation withAppDefinitionId(Long appDefinitionId) {
        this.appDefinitionId = appDefinitionId;
        return this;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public ProcessInstanceQueryRepresentation withProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
        return this;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public ProcessInstanceQueryRepresentation withProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
        return this;
    }
    
    public Integer getPage() {
        return page;
    }
    
    public void setPage(Integer page) {
        this.page = page;
    }
    
    public ProcessInstanceQueryRepresentation withPage(Integer page) {
        this.page = page;
        return this;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public ProcessInstanceQueryRepresentation withSize(Integer size) {
        this.size = size;
        return this;
    }

    public Sort getSort() {
        return sort;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }

    public ProcessInstanceQueryRepresentation withSort(Sort sort) {
        this.sort = sort;
        return this;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public ProcessInstanceQueryRepresentation withStart(Integer start) {
        this.start = start;
        return this;
    }
    
    public State getState() {
        return state;
    }
    
    public void setState(State state) {
        this.state = state;
    }
    
    public ProcessInstanceQueryRepresentation withState(State state) {
        this.state = state;
        return this;
    }

}
