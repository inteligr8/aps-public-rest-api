

package com.inteligr8.alfresco.activiti.model;

import java.time.OffsetDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "taskId",
    "parentTaskId",
    "taskName",
    "taskNameLike",
    "taskDefinitionKey",
    "taskDefinitionKeyLike",
    "finished",
    "taskPriority",
    "taskMinPriority",
    "taskMaxPriority",
    "taskAssignee",
    "taskAssigneeLike",
    "taskOwner",
    "taskOwnerLike",
    "taskInvolvedUser",
    "taskCandidateGroup",
    "taskCreatedOn",
    "taskCreatedAfter",
    "taskCreatedBefore",
    "dueDate",
    "dueDateAfter",
    "dueDateBefore",
    "withoutDueDate",
    "taskCompletedOn",
    "taskCompletedAfter",
    "taskCompletedBefore",
    "taskDeleteReason",
    "taskDeleteReasonLike",
    "taskDescription",
    "taskDescriptionLike",
    "processBusinessKey",
    "processBusinessKeyLike",
    "processDefinitionId",
    "processDefinitionKey",
    "processDefinitionKeyLike",
    "processDefinitionName",
    "processDefinitionNameLike",
    "processFinished",
    "processInstanceId",
    "includeProcessVariables",
    "includeTaskLocalVariables",
    "processVariables",
    "taskVariables",
    "size",
    "sort",
    "order",
    "start",
    "tenantId",
    "tenantIdLike",
    "withoutTenantId",
})
public class HistoricTaskQueryRepresentation {

    @JsonProperty("taskId")
    private String taskId;
    @JsonProperty("parentTaskId")
    private String parentTaskId;
    @JsonProperty("taskName")
    private String taskName;
    @JsonProperty("taskNameLike")
    private String taskNameLike;
    @JsonProperty("taskDefinitionKey")
    private String taskDefinitionKey;
    @JsonProperty("taskDefinitionKeyLike")
    private String taskDefinitionKeyLike;
    @JsonProperty("finished")
    private Boolean finished;
    @JsonProperty("taskPriority")
    private Integer taskPriority;
    @JsonProperty("taskMinPriority")
    private Integer taskMinPriority;
    @JsonProperty("taskMaxPriority")
    private Integer taskMaxPriority;
    @JsonProperty("taskAssignee")
    private String taskAssignee;
    @JsonProperty("taskAssigneeLike")
    private String taskAssigneeLike;
    @JsonProperty("taskOwner")
    private String taskOwner;
    @JsonProperty("taskOwnerLike")
    private String taskOwnerLike;
    @JsonProperty("taskInvolvedUser")
    private String taskInvolvedUser;
    @JsonProperty("taskCandidateGroup")
    private String taskCandidateGroup;
    @JsonProperty("taskCreatedOn")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime taskCreated;
    @JsonProperty("taskCreatedBefore")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime taskCreatedBefore;
    @JsonProperty("taskCreatedAfter")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime taskCreatedAfter;
    @JsonProperty("dueDate")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime dueDate;
    @JsonProperty("dueDateBefore")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime dueDateBefore;
    @JsonProperty("dueDateAfter")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime dueDateAfter;
    @JsonProperty("withoutDueDate")
    private Boolean withoutDueDate;
    @JsonProperty("taskCompletedOn")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime taskCompleted;
    @JsonProperty("taskCompletedBefore")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime taskCompletedBefore;
    @JsonProperty("taskCompletedAfter")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime taskCompletedAfter;
    @JsonProperty("taskDeleteReason")
    private String taskDeleteReason;
    @JsonProperty("taskDeleteReasonLike")
    private String taskDeleteReasonLike;
    @JsonProperty("taskDescription")
    private String taskDescription;
    @JsonProperty("taskDescriptionLike")
    private String taskDescriptionLike;
    @JsonProperty("processBusinessKey")
    private String processBusinessKey;
    @JsonProperty("processBusinessKeyLike")
    private String processBusinessKeyLike;
    @JsonProperty("processDefinitionId")
    private String processDefinitionId;
    @JsonProperty("processDefinitionKey")
    private String processDefinitionKey;
    @JsonProperty("processDefinitionKeyLike")
    private String processDefinitionKeyLike;
    @JsonProperty("processDefinitionName")
    private String processDefinitionName;
    @JsonProperty("processDefinitionNameLike")
    private String processDefinitionNameLike;
    @JsonProperty("processFinished")
    private Boolean processFinished;
    @JsonProperty("processInstanceId")
    private String processInstanceId;
    @JsonProperty("includeProcessVariables")
    private Boolean includeProcessVariables;
    @JsonProperty("includeTaskLocalVariables")
    private Boolean includeTaskLocalVariables;
    @JsonProperty("taskVariables")
    private List<QueryVariable> taskVariables;
    @JsonProperty("processVariables")
    private List<QueryVariable> processVariables;
    @JsonProperty("size")
    private Integer size;
    @JsonProperty("sort")
    private String sort;
    @JsonProperty("order")
    private String order;
    @JsonProperty("start")
    private Integer start;
    @JsonProperty("tenantId")
    private String tenantId;
    @JsonProperty("tenantIdLike")
    private String tenantIdLike;
    @JsonProperty("withoutTenantId")
    private Boolean withoutTenantId;

    /**
     * No args constructor for use in serialization
     */
    public HistoricTaskQueryRepresentation() {
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public HistoricTaskQueryRepresentation withTaskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    public String getParentTaskId() {
        return parentTaskId;
    }

    public void setParentTaskId(String parentTaskId) {
        this.parentTaskId = parentTaskId;
    }

	public HistoricTaskQueryRepresentation withParentTaskId(String parentTaskId) {
        this.parentTaskId = parentTaskId;
        return this;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

	public HistoricTaskQueryRepresentation withTaskName(String taskName) {
        this.taskName = taskName;
        return this;
    }

    public String getTaskNameLike() {
        return taskNameLike;
    }

    public void setTaskNameLike(String taskNameLike) {
        this.taskNameLike = taskNameLike;
    }

	public HistoricTaskQueryRepresentation withTaskNameLike(String taskNameLike) {
        this.taskNameLike = taskNameLike;
        return this;
    }

    public String getTaskDefinitionKey() {
        return taskDefinitionKey;
    }

    public void setTaskDefinitionKey(String taskDefinitionKey) {
        this.taskDefinitionKey = taskDefinitionKey;
    }

	public HistoricTaskQueryRepresentation withTaskDefinitionKey(String taskDefinitionKey) {
        this.taskDefinitionKey = taskDefinitionKey;
        return this;
    }

    public String getTaskDefinitionKeyLike() {
        return taskDefinitionKeyLike;
    }

    public void setTaskDefinitionKeyLike(String taskDefinitionKeyLike) {
        this.taskDefinitionKeyLike = taskDefinitionKeyLike;
    }

	public HistoricTaskQueryRepresentation withTaskDefinitionKeyLike(String taskDefinitionKeyLike) {
        this.taskDefinitionKeyLike = taskDefinitionKeyLike;
        return this;
    }

    public Boolean getFinished() {
        return finished;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }

	public HistoricTaskQueryRepresentation withFinished(Boolean finished) {
        this.finished = finished;
        return this;
    }

    public Integer getTaskPriority() {
        return taskPriority;
    }

    public void setTaskPriority(Integer taskPriority) {
        this.taskPriority = taskPriority;
    }

	public HistoricTaskQueryRepresentation withTaskPriority(Integer taskPriority) {
        this.taskPriority = taskPriority;
        return this;
    }

    public Integer getTaskMinPriority() {
        return taskMinPriority;
    }

    public void setTaskMinPriority(Integer taskMinPriority) {
        this.taskMinPriority = taskMinPriority;
    }

	public HistoricTaskQueryRepresentation withTaskMinPriority(Integer taskMinPriority) {
        this.taskMinPriority = taskMinPriority;
        return this;
    }

    public Integer getTaskMaxPriority() {
        return taskMaxPriority;
    }

    public void setTaskMaxPriority(Integer taskMaxPriority) {
        this.taskMaxPriority = taskMaxPriority;
    }

	public HistoricTaskQueryRepresentation withTaskMaxPriority(Integer taskMaxPriority) {
        this.taskMaxPriority = taskMaxPriority;
        return this;
    }

    public String getTaskAssignee() {
        return taskAssignee;
    }

    public void setTaskAssignee(String taskAssignee) {
        this.taskAssignee = taskAssignee;
    }

	public HistoricTaskQueryRepresentation withTaskAssignee(String taskAssignee) {
        this.taskAssignee = taskAssignee;
        return this;
    }

    public String getTaskAssigneeLike() {
        return taskAssigneeLike;
    }

    public void setTaskAssigneeLike(String taskAssigneeLike) {
        this.taskAssigneeLike = taskAssigneeLike;
    }

	public HistoricTaskQueryRepresentation withTaskAssigneeLike(String taskAssigneeLike) {
        this.taskAssigneeLike = taskAssigneeLike;
        return this;
    }

    public String getTaskOwner() {
        return taskOwner;
    }

    public void setTaskOwner(String taskOwner) {
        this.taskOwner = taskOwner;
    }

	public HistoricTaskQueryRepresentation withTaskOwner(String taskOwner) {
        this.taskOwner = taskOwner;
        return this;
    }

    public String getTaskOwnerLike() {
        return taskOwnerLike;
    }

    public void setTaskOwnerLike(String taskOwnerLike) {
        this.taskOwnerLike = taskOwnerLike;
    }

	public HistoricTaskQueryRepresentation withTaskOwnerLike(String taskOwnerLike) {
        this.taskOwnerLike = taskOwnerLike;
        return this;
    }

    public String getTaskInvolvedUser() {
        return taskInvolvedUser;
    }

    public void setTaskInvolvedUser(String taskInvolvedUser) {
        this.taskInvolvedUser = taskInvolvedUser;
    }

	public HistoricTaskQueryRepresentation withTaskInvolvedUser(String taskInvolvedUser) {
        this.taskInvolvedUser = taskInvolvedUser;
        return this;
    }

    public String getTaskCandidateGroup() {
        return taskCandidateGroup;
    }

    public void setTaskCandidateGroup(String taskCandidateGroup) {
        this.taskCandidateGroup = taskCandidateGroup;
    }

	public HistoricTaskQueryRepresentation withTaskCandidateGroup(String taskCandidateGroup) {
        this.taskCandidateGroup = taskCandidateGroup;
        return this;
    }

    public OffsetDateTime getTaskCreated() {
        return taskCreated;
    }

    public void setTaskCreated(OffsetDateTime taskCreated) {
        this.taskCreated = taskCreated;
    }

	public HistoricTaskQueryRepresentation withTaskCreated(OffsetDateTime taskCreated) {
        this.taskCreated = taskCreated;
        return this;
    }

    public OffsetDateTime getTaskCreatedBefore() {
        return taskCreatedBefore;
    }

    public void setTaskCreatedBefore(OffsetDateTime taskCreatedBefore) {
        this.taskCreatedBefore = taskCreatedBefore;
    }

	public HistoricTaskQueryRepresentation withTaskCreatedBefore(OffsetDateTime taskCreatedBefore) {
        this.taskCreatedBefore = taskCreatedBefore;
        return this;
    }

    public OffsetDateTime getTaskCreatedAfter() {
        return taskCreatedAfter;
    }

    public void setTaskCreatedAfter(OffsetDateTime taskCreatedAfter) {
        this.taskCreatedAfter = taskCreatedAfter;
    }

	public HistoricTaskQueryRepresentation withTaskCreatedAfter(OffsetDateTime taskCreatedAfter) {
        this.taskCreatedAfter = taskCreatedAfter;
        return this;
    }

    public OffsetDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(OffsetDateTime dueDate) {
        this.dueDate = dueDate;
    }

	public HistoricTaskQueryRepresentation withDueDate(OffsetDateTime dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    public OffsetDateTime getDueDateBefore() {
        return dueDateBefore;
    }

    public void setDueDateBefore(OffsetDateTime dueDateBefore) {
        this.dueDateBefore = dueDateBefore;
    }

	public HistoricTaskQueryRepresentation withDueDateBefore(OffsetDateTime dueDateBefore) {
        this.dueDateBefore = dueDateBefore;
        return this;
    }

    public OffsetDateTime getDueDateAfter() {
        return dueDateAfter;
    }

    public void setDueDateAfter(OffsetDateTime dueDateAfter) {
        this.dueDateAfter = dueDateAfter;
    }

	public HistoricTaskQueryRepresentation withDueDateAfter(OffsetDateTime dueDateAfter) {
        this.dueDateAfter = dueDateAfter;
        return this;
    }

    public Boolean getWithoutDueDate() {
        return withoutDueDate;
    }

    public void setWithoutDueDate(Boolean withoutDueDate) {
        this.withoutDueDate = withoutDueDate;
    }

	public HistoricTaskQueryRepresentation withWithoutDueDate(Boolean withoutDueDate) {
        this.withoutDueDate = withoutDueDate;
        return this;
    }

    public OffsetDateTime getTaskCompleted() {
        return taskCompleted;
    }

    public void setTaskCompleted(OffsetDateTime taskCompleted) {
        this.taskCompleted = taskCompleted;
    }

	public HistoricTaskQueryRepresentation withTaskCompleted(OffsetDateTime taskCompleted) {
        this.taskCompleted = taskCompleted;
        return this;
    }

    public OffsetDateTime getTaskCompletedBefore() {
        return taskCompletedBefore;
    }

    public void setTaskCompletedBefore(OffsetDateTime taskCompletedBefore) {
        this.taskCompletedBefore = taskCompletedBefore;
    }

	public HistoricTaskQueryRepresentation withTaskCompletedBefore(OffsetDateTime taskCompletedBefore) {
        this.taskCompletedBefore = taskCompletedBefore;
        return this;
    }

    public OffsetDateTime getTaskCompletedAfter() {
        return taskCompletedAfter;
    }

    public void setTaskCompletedAfter(OffsetDateTime taskCompletedAfter) {
        this.taskCompletedAfter = taskCompletedAfter;
    }

	public HistoricTaskQueryRepresentation withTaskCompletedAfter(OffsetDateTime taskCompletedAfter) {
        this.taskCompletedAfter = taskCompletedAfter;
        return this;
    }

    public String getTaskDeleteReason() {
        return taskDeleteReason;
    }

    public void setTaskDeleteReason(String taskDeleteReason) {
        this.taskDeleteReason = taskDeleteReason;
    }

	public HistoricTaskQueryRepresentation withTaskDeleteReason(String taskDeleteReason) {
        this.taskDeleteReason = taskDeleteReason;
        return this;
    }

    public String getTaskDeleteReasonLike() {
        return taskDeleteReasonLike;
    }

    public void setTaskDeleteReasonLike(String taskDeleteReasonLike) {
        this.taskDeleteReasonLike = taskDeleteReasonLike;
    }

	public HistoricTaskQueryRepresentation withTaskDeleteReasonLike(String taskDeleteReasonLike) {
        this.taskDeleteReasonLike = taskDeleteReasonLike;
        return this;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

	public HistoricTaskQueryRepresentation withTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
        return this;
    }

    public String getTaskDescriptionLike() {
        return taskDescriptionLike;
    }

    public void setTaskDescriptionLike(String taskDescriptionLike) {
        this.taskDescriptionLike = taskDescriptionLike;
    }

	public HistoricTaskQueryRepresentation withTaskDescriptionLike(String taskDescriptionLike) {
        this.taskDescriptionLike = taskDescriptionLike;
        return this;
    }

    public String getProcessBusinessKey() {
        return processBusinessKey;
    }

    public void setProcessBusinessKey(String processBusinessKey) {
        this.processBusinessKey = processBusinessKey;
    }

	public HistoricTaskQueryRepresentation withProcessBusinessKey(String processBusinessKey) {
        this.processBusinessKey = processBusinessKey;
        return this;
    }

    public String getProcessBusinessKeyLike() {
        return processBusinessKeyLike;
    }

    public void setProcessBusinessKeyLike(String processBusinessKeyLike) {
        this.processBusinessKeyLike = processBusinessKeyLike;
    }

	public HistoricTaskQueryRepresentation withProcessBusinessKeyLike(String processBusinessKeyLike) {
        this.processBusinessKeyLike = processBusinessKeyLike;
        return this;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

	public HistoricTaskQueryRepresentation withProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
        return this;
    }

    public String getProcessDefinitionKey() {
        return processDefinitionKey;
    }

    public void setProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
    }

	public HistoricTaskQueryRepresentation withProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
        return this;
    }

    public String getProcessDefinitionKeyLike() {
        return processDefinitionKeyLike;
    }

    public void setProcessDefinitionKeyLike(String processDefinitionKeyLike) {
        this.processDefinitionKeyLike = processDefinitionKeyLike;
    }

	public HistoricTaskQueryRepresentation withProcessDefinitionKeyLike(String processDefinitionKeyLike) {
        this.processDefinitionKeyLike = processDefinitionKeyLike;
        return this;
    }

    public String getProcessDefinitionName() {
        return processDefinitionName;
    }

    public void setProcessDefinitionName(String processDefinitionName) {
        this.processDefinitionName = processDefinitionName;
    }

	public HistoricTaskQueryRepresentation withProcessDefinitionName(String processDefinitionName) {
        this.processDefinitionName = processDefinitionName;
        return this;
    }

    public String getProcessDefinitionNameLike() {
        return processDefinitionNameLike;
    }

    public void setProcessDefinitionNameLike(String processDefinitionNameLike) {
        this.processDefinitionNameLike = processDefinitionNameLike;
    }

	public HistoricTaskQueryRepresentation withProcessDefinitionNameLike(String processDefinitionNameLike) {
        this.processDefinitionNameLike = processDefinitionNameLike;
        return this;
    }

    public Boolean getProcessFinished() {
        return processFinished;
    }

    public void setProcessFinished(Boolean processFinished) {
        this.processFinished = processFinished;
    }

	public HistoricTaskQueryRepresentation withProcessFinished(Boolean processFinished) {
        this.processFinished = processFinished;
        return this;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

	public HistoricTaskQueryRepresentation withProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
        return this;
    }

    public Boolean getIncludeProcessVariables() {
        return includeProcessVariables;
    }

    public void setIncludeProcessVariables(Boolean includeProcessVariables) {
        this.includeProcessVariables = includeProcessVariables;
    }

	public HistoricTaskQueryRepresentation withIncludeProcessVariables(Boolean includeProcessVariables) {
        this.includeProcessVariables = includeProcessVariables;
        return this;
    }

    public Boolean getIncludeTaskLocalVariables() {
        return includeTaskLocalVariables;
    }

    public void setIncludeTaskLocalVariables(Boolean includeTaskLocalVariables) {
        this.includeTaskLocalVariables = includeTaskLocalVariables;
    }

	public HistoricTaskQueryRepresentation withIncludeTaskLocalVariables(Boolean includeTaskLocalVariables) {
        this.includeTaskLocalVariables = includeTaskLocalVariables;
        return this;
    }

    public List<QueryVariable> getTaskVariables() {
        return taskVariables;
    }

    public void setTaskVariables(List<QueryVariable> taskVariables) {
        this.taskVariables = taskVariables;
    }

	public HistoricTaskQueryRepresentation withTaskVariables(List<QueryVariable> taskVariables) {
        this.taskVariables = taskVariables;
        return this;
    }

    public List<QueryVariable> getProcessVariables() {
        return processVariables;
    }

    public void setProcessVariables(List<QueryVariable> processVariables) {
        this.processVariables = processVariables;
    }

	public HistoricTaskQueryRepresentation withProcessVariables(List<QueryVariable> processVariables) {
        this.processVariables = processVariables;
        return this;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

	public HistoricTaskQueryRepresentation withSize(Integer size) {
        this.size = size;
        return this;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

	public HistoricTaskQueryRepresentation withSort(String sort) {
        this.sort = sort;
        return this;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

	public HistoricTaskQueryRepresentation withOrder(String order) {
        this.order = order;
        return this;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

	public HistoricTaskQueryRepresentation withStart(Integer start) {
        this.start = start;
        return this;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

	public HistoricTaskQueryRepresentation withTenantId(String tenantId) {
        this.tenantId = tenantId;
        return this;
    }

    public String getTenantIdLike() {
        return tenantIdLike;
    }

    public void setTenantIdLike(String tenantIdLike) {
        this.tenantIdLike = tenantIdLike;
    }

	public HistoricTaskQueryRepresentation withTenantIdLike(String tenantIdLike) {
        this.tenantIdLike = tenantIdLike;
        return this;
    }

    public Boolean getWithoutTenantId() {
        return withoutTenantId;
    }

    public void setWithoutTenantId(Boolean withoutTenantId) {
        this.withoutTenantId = withoutTenantId;
    }

	public HistoricTaskQueryRepresentation withWithoutTenantId(Boolean withoutTenantId) {
        this.withoutTenantId = withoutTenantId;
        return this;
    }

}
