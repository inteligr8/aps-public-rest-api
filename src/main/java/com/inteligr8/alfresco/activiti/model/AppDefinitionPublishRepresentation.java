
package com.inteligr8.alfresco.activiti.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * AppDefinitionPublishRepresentation
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "comment",
    "force"
})
public class AppDefinitionPublishRepresentation {

    @JsonProperty("comment")
    private String comment;
    @JsonProperty("force")
    private Boolean force;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("comment")
    public String getComment() {
        return comment;
    }

    @JsonProperty("comment")
    public void setComment(String comment) {
        this.comment = comment;
    }

    public AppDefinitionPublishRepresentation withComment(String comment) {
        this.comment = comment;
        return this;
    }

    @JsonProperty("force")
    public Boolean getForce() {
        return force;
    }

    @JsonProperty("force")
    public void setForce(Boolean force) {
        this.force = force;
    }

    public AppDefinitionPublishRepresentation withForce(Boolean force) {
        this.force = force;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public AppDefinitionPublishRepresentation withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}
