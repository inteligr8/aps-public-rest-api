package com.inteligr8.alfresco.activiti.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "operation",
    "type",
    "value"
})
public class QueryVariable {

    @JsonProperty("name")
    private String name;
    @JsonProperty("operation")
    private String operation;
    @JsonProperty("type")
    private String type;
    @JsonProperty("value")
    private Object value;

    /**
     * No args constructor for use in serialization
     */
    public QueryVariable() {
    }

    public QueryVariable(String name, String operation, String type, Object value) {
        this.name = name;
        this.operation = operation;
        this.type = type;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public QueryVariable withName(String name) {
        this.name = name;
        return this;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public QueryVariable withOperation(String operation) {
        this.operation = operation;
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public QueryVariable withType(String type) {
        this.type = type;
        return this;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public QueryVariable withValue(Object value) {
        this.value = value;
        return this;
    }
    
}
