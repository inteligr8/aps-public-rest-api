package com.inteligr8.alfresco.activiti.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "type",
    "value"
})
public class ProcessInstanceVariable {

    @JsonProperty("id")
    private String id;
    @JsonProperty("type")
    private String type;
    @JsonProperty("value")
    private Object value;

    /**
     * No args constructor for use in serialization
     */
    public ProcessInstanceVariable() {
    }

    public ProcessInstanceVariable(String id, String type, Object value) {
        this.id = id;
        this.type = type;
        this.value = value;
    }
    
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public ProcessInstanceVariable withId(String id) {
        this.id = id;
        return this;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ProcessInstanceVariable withType(String type) {
        this.type = type;
        return this;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public ProcessInstanceVariable withValue(Object value) {
        this.value = value;
        return this;
    }
    
}
