package com.inteligr8.alfresco.activiti.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PermissionLight {
	
	@JsonProperty
	private String id;
	@JsonProperty
	private PermissionLevel permission;
	@JsonProperty
	private Long personId;
	@JsonProperty
	private Long groupId;

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public PermissionLight withId(String id) {
		this.setId(id);
		return this;
	}
	
	public PermissionLevel getPermission() {
		return permission;
	}
	
	public void setPermission(PermissionLevel permission) {
		this.permission = permission;
	}
	
	public PermissionLight withPermission(PermissionLevel permission) {
		this.setPermission(permission);
		return this;
	}

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}
	
	public PermissionLight withPersonId(Long personId) {
		this.setPersonId(personId);
		return this;
	}

	public Long getGroupId() {
		return groupId;
	}

	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	
	public PermissionLight withGroupId(Long groupId) {
		this.setGroupId(groupId);
		return this;
	}
	

}
