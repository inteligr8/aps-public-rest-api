

package com.inteligr8.alfresco.activiti.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "filterId",
    "filter",
    "appDefinitionId",
    "page",
    "size"
})
public class ProcessInstanceFilterRepresentation {

    @JsonProperty("filterId")
    private Long filterId;
    @JsonProperty("filter")
    private ProcessInstanceFilter filter;
    @JsonProperty("appDefinitionId")
    private Long appDefinitionId;
    @JsonProperty("page")
    private Integer page;
    @JsonProperty("size")
    private Integer size;

    /**
     * No args constructor for use in serialization
     */
    public ProcessInstanceFilterRepresentation() {
    }
    
    public Long getFilterId() {
        return filterId;
    }
    
    public void setFilterId(Long filterId) {
        this.filterId = filterId;
    }
    
    public ProcessInstanceFilterRepresentation withFilterId(Long filterId) {
        this.filterId = filterId;
        return this;
    }
    
    public ProcessInstanceFilter getFilter() {
        return filter;
    }
    
    public void setFilter(ProcessInstanceFilter filter) {
        this.filter = filter;
    }
    
    public ProcessInstanceFilterRepresentation withFilter(ProcessInstanceFilter filter) {
        this.filter = filter;
        return this;
    }

    public Long getAppDefinitionId() {
        return appDefinitionId;
    }

    public void setAppDefinitionId(Long appDefinitionId) {
        this.appDefinitionId = appDefinitionId;
    }

    public ProcessInstanceFilterRepresentation withAppDefinitionId(Long appDefinitionId) {
        this.appDefinitionId = appDefinitionId;
        return this;
    }
    
    public Integer getPage() {
        return page;
    }
    
    public void setPage(Integer page) {
        this.page = page;
    }
    
    public ProcessInstanceFilterRepresentation withPage(Integer page) {
        this.page = page;
        return this;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public ProcessInstanceFilterRepresentation withSize(Integer size) {
        this.size = size;
        return this;
    }

}
