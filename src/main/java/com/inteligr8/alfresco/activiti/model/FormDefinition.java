
package com.inteligr8.alfresco.activiti.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.inteligr8.activiti.model.Datum;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FormDefinition extends Datum {

    @JsonProperty("className")
    private String className;
    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("processDefinitionId")
    private String processDefinitionId;
    @JsonProperty("processDefinitionKey")
    private String processDefinitionKey;
    @JsonProperty("processDefinitionName")
    private String processDefinitionName;
    @JsonProperty("outcomes")
    private List<FormOutcome> outcomes = new ArrayList<>();
    @JsonProperty("selectedOutcome")
    private String selectedOutcome;
    @JsonProperty("taskDefinitionKey")
    private String taskDefinitionKey;
    @JsonProperty("taskId")
    private String taskId;
    @JsonProperty("taskName")
    private String taskName;
    @JsonProperty("variables")
    private List<FormVariable> variables = new ArrayList<>();

    /**
     * No args constructor for use in serialization
     */
    public FormDefinition() {
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

	public FormDefinition withClassName(String className) {
        this.className = className;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public FormDefinition withId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	public FormDefinition withName(String name) {
        this.name = name;
        return this;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

	public FormDefinition withProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
        return this;
    }

    public String getProcessDefinitionKey() {
        return processDefinitionKey;
    }

    public void setProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
    }

	public FormDefinition withProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
        return this;
    }

    public String getProcessDefinitionName() {
        return processDefinitionName;
    }

    public void setProcessDefinitionName(String processDefinitionName) {
        this.processDefinitionName = processDefinitionName;
    }

	public FormDefinition withProcessDefinitionName(String processDefinitionName) {
        this.processDefinitionName = processDefinitionName;
        return this;
    }

    public List<FormOutcome> getOutcomes() {
        return outcomes;
    }

    public void setOutcomes(List<FormOutcome> outcomes) {
        this.outcomes = outcomes;
    }

	public FormDefinition withOutcomes(List<FormOutcome> outcomes) {
        this.outcomes = outcomes;
        return this;
    }

    public String getSelectedOutcome() {
        return selectedOutcome;
    }

    public void setSelectedOutcome(String selectedOutcome) {
        this.selectedOutcome = selectedOutcome;
    }

	public FormDefinition withSelectedOutcome(String selectedOutcome) {
        this.selectedOutcome = selectedOutcome;
        return this;
    }

    public String getTaskDefinitionKey() {
        return taskDefinitionKey;
    }

    public void setTaskDefinitionKey(String taskDefinitionKey) {
        this.taskDefinitionKey = taskDefinitionKey;
    }

	public FormDefinition withTaskDefinitionKey(String taskDefinitionKey) {
        this.taskDefinitionKey = taskDefinitionKey;
        return this;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

	public FormDefinition withTaskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

	public FormDefinition withTaskName(String taskName) {
        this.taskName = taskName;
        return this;
    }

    public List<FormVariable> getVariables() {
        return variables;
    }

    public void setVariables(List<FormVariable> variables) {
        this.variables = variables;
    }

	public FormDefinition withVariables(List<FormVariable> variables) {
        this.variables = variables;
        return this;
    }

}
