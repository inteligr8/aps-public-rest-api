

package com.inteligr8.alfresco.activiti.model;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "text",
    "assignment",
    "taskId",
    "dueAfter",
    "dueBefore",
    "processInstanceId",
    "processDefinitionId",
    "appDefinitionId",
    "includeProcessInstance",
    "includeProcessVariables",
    "includeTaskLocalVariables",
    "page",
    "size",
    "sort",
    "start",
    "state"
})
public class TaskQueryRepresentation {
    
    public enum Sort {
        @JsonProperty("created-asc")
        CreatedAsc,
        @JsonProperty("created-desc")
        CreatedDesc,
        @JsonProperty("due-asc")
        DueAsc,
        @JsonProperty("due-desc")
        DueDesc,
    }
    
    public enum State {
        @JsonProperty("all")
        All,
        @JsonProperty("completed")
        Completed,
        @JsonProperty("active")
        Active
    }

    @JsonProperty("text")
    private String text;
    @JsonProperty("dueBefore")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime dueBefore;
    @JsonProperty("dueAfter")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime dueAfter;
    @JsonProperty("taskId")
    private String taskId;
    @JsonProperty("processInstanceId")
    private String processInstanceId;
    @JsonProperty("processDefinitionId")
    private String processDefinitionId;
    @JsonProperty("appDefinitionId")
    private Long appDefinitionId;
    @JsonProperty("page")
    private Integer page;
    @JsonProperty("size")
    private Integer size;
    @JsonProperty("sort")
    private Sort sort;
    @JsonProperty("start")
    private Integer start;
    @JsonProperty("state")
    private State state;

    /**
     * No args constructor for use in serialization
     */
    public TaskQueryRepresentation() {
    }
    
    public String getText() {
        return text;
    }
    
    public void setText(String text) {
        this.text = text;
    }
    
    public TaskQueryRepresentation withText(String text) {
        this.text = text;
        return this;
    }
    
    public OffsetDateTime getDueAfter() {
        return dueAfter;
    }
    
    public void setDueAfter(OffsetDateTime dueAfter) {
        this.dueAfter = dueAfter;
    }
    
    public TaskQueryRepresentation withDueAfter(OffsetDateTime dueAfter) {
        this.dueAfter = dueAfter;
        return this;
    }
    
    public OffsetDateTime getDueBefore() {
        return dueBefore;
    }
    
    public void setDueBefore(OffsetDateTime dueBefore) {
        this.dueBefore = dueBefore;
    }
    
    public TaskQueryRepresentation withDueBefore(OffsetDateTime dueBefore) {
        this.dueBefore = dueBefore;
        return this;
    }
    
    public String getTaskId() {
        return taskId;
    }
    
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }
    
    public TaskQueryRepresentation withTaskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    public Long getAppDefinitionId() {
        return appDefinitionId;
    }

    public void setAppDefinitionId(Long appDefinitionId) {
        this.appDefinitionId = appDefinitionId;
    }

    public TaskQueryRepresentation withAppDefinitionId(Long appDefinitionId) {
        this.appDefinitionId = appDefinitionId;
        return this;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public TaskQueryRepresentation withProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
        return this;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public TaskQueryRepresentation withProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
        return this;
    }
    
    public Integer getPage() {
        return page;
    }
    
    public void setPage(Integer page) {
        this.page = page;
    }
    
    public TaskQueryRepresentation withPage(Integer page) {
        this.page = page;
        return this;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public TaskQueryRepresentation withSize(Integer size) {
        this.size = size;
        return this;
    }

    public Sort getSort() {
        return sort;
    }

    public void setSort(Sort sort) {
        this.sort = sort;
    }

    public TaskQueryRepresentation withSort(Sort sort) {
        this.sort = sort;
        return this;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public TaskQueryRepresentation withStart(Integer start) {
        this.start = start;
        return this;
    }
    
    public State getState() {
        return state;
    }
    
    public void setState(State state) {
        this.state = state;
    }
    
    public TaskQueryRepresentation withState(State state) {
        this.state = state;
        return this;
    }

}
