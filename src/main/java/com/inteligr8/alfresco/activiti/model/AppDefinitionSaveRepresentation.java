
package com.inteligr8.alfresco.activiti.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * AppDefinitionSaveRepresentation
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "appDefinition",
    "force",
    "publish"
})
public class AppDefinitionSaveRepresentation {

    /**
     * AppDefinitionRepresentation
     * <p>
     * 
     * 
     */
    @JsonProperty("appDefinition")
    private AppDefinition appDefinition;
    @JsonProperty("force")
    private Boolean force;
    @JsonProperty("publish")
    private Boolean publish;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * AppDefinitionRepresentation
     * <p>
     * 
     * 
     */
    @JsonProperty("appDefinition")
    public AppDefinition getAppDefinition() {
        return appDefinition;
    }

    /**
     * AppDefinitionRepresentation
     * <p>
     * 
     * 
     */
    @JsonProperty("appDefinition")
    public void setAppDefinition(AppDefinition appDefinition) {
        this.appDefinition = appDefinition;
    }

    public AppDefinitionSaveRepresentation withAppDefinition(AppDefinition appDefinition) {
        this.appDefinition = appDefinition;
        return this;
    }

    @JsonProperty("force")
    public Boolean getForce() {
        return force;
    }

    @JsonProperty("force")
    public void setForce(Boolean force) {
        this.force = force;
    }

    public AppDefinitionSaveRepresentation withForce(Boolean force) {
        this.force = force;
        return this;
    }

    @JsonProperty("publish")
    public Boolean getPublish() {
        return publish;
    }

    @JsonProperty("publish")
    public void setPublish(Boolean publish) {
        this.publish = publish;
    }

    public AppDefinitionSaveRepresentation withPublish(Boolean publish) {
        this.publish = publish;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public AppDefinitionSaveRepresentation withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}
