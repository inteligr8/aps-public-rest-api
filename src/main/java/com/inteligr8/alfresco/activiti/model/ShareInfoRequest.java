package com.inteligr8.alfresco.activiti.model;

import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShareInfoRequest {
	
	@JsonProperty
	private List<PermissionLight> added = new LinkedList<>();
	@JsonProperty
	private List<PermissionLight> removed = new LinkedList<>();
	@JsonProperty
	private List<PermissionLight> updated = new LinkedList<>();
	
	
	
	public List<PermissionLight> getAdded() {
		return added;
	}
	
	public void setAdded(List<PermissionLight> added) {
		this.added = added;
	}
	
	public List<PermissionLight> getRemoved() {
		return removed;
	}
	
	public void setRemoved(List<PermissionLight> removed) {
		this.removed = removed;
	}
	
	public List<PermissionLight> getUpdated() {
		return updated;
	}
	
	public void setUpdated(List<PermissionLight> updated) {
		this.updated = updated;
	}

}
