package com.inteligr8.alfresco.activiti.model;

import java.util.HashMap;

public class QuickMap<K, V> extends HashMap<K, V> {
	
	private static final long serialVersionUID = 6480454666954785899L;

	public QuickMap(int initialCapacity) {
		super(initialCapacity);
	}
	
	public QuickMap<K, V> with(K key, V value) {
		if (value != null)
			this.put(key, value);
		return this;
	}
	
	public QuickMap<K, V> withNull(K key, V value) {
		this.put(key, value);
		return this;
	}

}
