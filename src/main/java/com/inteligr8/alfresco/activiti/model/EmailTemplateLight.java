package com.inteligr8.alfresco.activiti.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EmailTemplateLight extends BaseTemplateLight {

    @JsonProperty
    private String systemTemplateId;
	@JsonProperty
	private String subject;
    
    public String getSystemTemplateId() {
        return systemTemplateId;
    }
    
    public void setSystemTemplateId(String systemTemplateId) {
        this.systemTemplateId = systemTemplateId;
    }
	
	public String getSubject() {
        return subject;
    }
	
	public void setSubject(String subject) {
        this.subject = subject;
    }

}
