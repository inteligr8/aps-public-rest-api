package com.inteligr8.alfresco.activiti.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "revisionVersion",
    "edition",
    "type",
    "majorVersion",
    "minorVersion"
})
public class AppVersion {

    @JsonProperty("revisionVersion")
    private String revisionVersion;
    @JsonProperty("edition")
    private String edition;
    @JsonProperty("type")
    private String type;
    @JsonProperty("majorVersion")
    private String majorVersion;
    @JsonProperty("minorVersion")
    private String minorVersion;

    /**
     * No args constructor for use in serialization
     */
    public AppVersion() {
    }
    
    public String getRevisionVersion() {
		return revisionVersion;
	}
    
    public void setRevisionVersion(String revisionVersion) {
		this.revisionVersion = revisionVersion;
	}
    
    public AppVersion withRevisionVersion(String revisionVersion) {
    	this.setRevisionVersion(revisionVersion);
    	return this;
    }
    
    public String getEdition() {
		return edition;
	}
    
    public void setEdition(String edition) {
		this.edition = edition;
	}
    
    public AppVersion withEdition(String edition) {
    	this.setEdition(edition);
    	return this;
    }
    
    public String getType() {
		return type;
	}
    
    public void setType(String type) {
		this.type = type;
	}
    
    public AppVersion withType(String type) {
    	this.setType(type);
    	return this;
    }
    
    public String getMajorVersion() {
		return majorVersion;
	}
    
    public void setMajorVersion(String majorVersion) {
		this.majorVersion = majorVersion;
	}
    
    public AppVersion withMajorVersion(String majorVersion) {
    	this.setMajorVersion(majorVersion);
    	return this;
    }
    
    public String getMinorVersion() {
		return minorVersion;
	}
    
    public void setMinorVersion(String minorVersion) {
		this.minorVersion = minorVersion;
	}
    
    public AppVersion withMinorVersion(String minorVersion) {
    	this.setMinorVersion(minorVersion);
    	return this;
    }

}
