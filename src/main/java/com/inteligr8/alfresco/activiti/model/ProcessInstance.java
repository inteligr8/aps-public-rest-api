

package com.inteligr8.alfresco.activiti.model;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;
import com.inteligr8.activiti.model.Datum;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name",
    "businessKey",
    "processDefinitionId",
    "tenantId",
    "started",
    "ended",
    "startedBy",
    "processDefinitionName",
    "processDefinitionDescription",
    "processDefinitionKey",
    "processDefinitionCategory",
    "processDefinitionVersion",
    "processDefinitionDeploymentId",
    "graphicalNotationDefined",
    "startFormDefined",
    "suspended",
    "variables"
})
public class ProcessInstance extends Datum {

    @JsonProperty("businessKey")
    private String businessKey;
    @JsonProperty("ended")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime ended;
    @JsonProperty("graphicalNotationDefined")
    private Boolean graphicalNotationDefined;
    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("processDefinitionCategory")
    private String processDefinitionCategory;
    @JsonProperty("processDefinitionDeploymentId")
    private String processDefinitionDeploymentId;
    @JsonProperty("processDefinitionDescription")
    private String processDefinitionDescription;
    @JsonProperty("processDefinitionId")
    private String processDefinitionId;
    @JsonProperty("processDefinitionKey")
    private String processDefinitionKey;
    @JsonProperty("processDefinitionName")
    private String processDefinitionName;
    @JsonProperty("processDefinitionVersion")
    private Long processDefinitionVersion;
    @JsonProperty("startFormDefined")
    private Boolean startFormDefined;
    @JsonProperty("started")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime started;
    @JsonProperty("startedBy")
    private UserLight startedBy;
    @JsonProperty("suspended")
    private Boolean suspended;
    @JsonProperty("tenantId")
    private String tenantId;
    @JsonProperty("variables")
    private List<Variable> variables = new ArrayList<Variable>();

    /**
     * No args constructor for use in serialization
     */
    public ProcessInstance() {
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public ProcessInstance withBusinessKey(String businessKey) {
        this.businessKey = businessKey;
        return this;
    }

    public OffsetDateTime getEnded() {
        return ended;
    }

    public void setEnded(OffsetDateTime ended) {
        this.ended = ended;
    }

    public ProcessInstance withEnded(OffsetDateTime ended) {
        this.ended = ended;
        return this;
    }

    public Boolean getGraphicalNotationDefined() {
        return graphicalNotationDefined;
    }

    public void setGraphicalNotationDefined(Boolean graphicalNotationDefined) {
        this.graphicalNotationDefined = graphicalNotationDefined;
    }

    public ProcessInstance withGraphicalNotationDefined(Boolean graphicalNotationDefined) {
        this.graphicalNotationDefined = graphicalNotationDefined;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProcessInstance withId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProcessInstance withName(String name) {
        this.name = name;
        return this;
    }

    public String getProcessDefinitionCategory() {
        return processDefinitionCategory;
    }

    public void setProcessDefinitionCategory(String processDefinitionCategory) {
        this.processDefinitionCategory = processDefinitionCategory;
    }

    public ProcessInstance withProcessDefinitionCategory(String processDefinitionCategory) {
        this.processDefinitionCategory = processDefinitionCategory;
        return this;
    }

    public String getProcessDefinitionDeploymentId() {
        return processDefinitionDeploymentId;
    }

    public void setProcessDefinitionDeploymentId(String processDefinitionDeploymentId) {
        this.processDefinitionDeploymentId = processDefinitionDeploymentId;
    }

    public ProcessInstance withProcessDefinitionDeploymentId(String processDefinitionDeploymentId) {
        this.processDefinitionDeploymentId = processDefinitionDeploymentId;
        return this;
    }

    public String getProcessDefinitionDescription() {
        return processDefinitionDescription;
    }

    public void setProcessDefinitionDescription(String processDefinitionDescription) {
        this.processDefinitionDescription = processDefinitionDescription;
    }

    public ProcessInstance withProcessDefinitionDescription(String processDefinitionDescription) {
        this.processDefinitionDescription = processDefinitionDescription;
        return this;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public ProcessInstance withProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
        return this;
    }

    public String getProcessDefinitionKey() {
        return processDefinitionKey;
    }

    public void setProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
    }

    public ProcessInstance withProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
        return this;
    }

    public String getProcessDefinitionName() {
        return processDefinitionName;
    }

    public void setProcessDefinitionName(String processDefinitionName) {
        this.processDefinitionName = processDefinitionName;
    }

    public ProcessInstance withProcessDefinitionName(String processDefinitionName) {
        this.processDefinitionName = processDefinitionName;
        return this;
    }

    public Long getProcessDefinitionVersion() {
        return processDefinitionVersion;
    }

    public void setProcessDefinitionVersion(Long processDefinitionVersion) {
        this.processDefinitionVersion = processDefinitionVersion;
    }

    public ProcessInstance withProcessDefinitionVersion(Long processDefinitionVersion) {
        this.processDefinitionVersion = processDefinitionVersion;
        return this;
    }

    public Boolean getStartFormDefined() {
        return startFormDefined;
    }

    public void setStartFormDefined(Boolean startFormDefined) {
        this.startFormDefined = startFormDefined;
    }

    public ProcessInstance withStartFormDefined(Boolean startFormDefined) {
        this.startFormDefined = startFormDefined;
        return this;
    }

    public OffsetDateTime getStarted() {
        return started;
    }

    public void setStarted(OffsetDateTime started) {
        this.started = started;
    }

    public ProcessInstance withStarted(OffsetDateTime started) {
        this.started = started;
        return this;
    }

    public UserLight getStartedBy() {
        return startedBy;
    }

    public void setStartedBy(UserLight startedBy) {
        this.startedBy = startedBy;
    }

    public ProcessInstance withStartedBy(UserLight startedBy) {
        this.startedBy = startedBy;
        return this;
    }

    public Boolean getSuspended() {
        return suspended;
    }

    public void setSuspended(Boolean suspended) {
        this.suspended = suspended;
    }

    public ProcessInstance withSuspended(Boolean suspended) {
        this.suspended = suspended;
        return this;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public ProcessInstance withTenantId(String tenantId) {
        this.tenantId = tenantId;
        return this;
    }

    public List<Variable> getVariables() {
        return variables;
    }

    public void setVariables(List<Variable> variables) {
        this.variables = variables;
    }

    public ProcessInstance withVariables(List<Variable> variables) {
        this.variables = variables;
        return this;
    }

}
