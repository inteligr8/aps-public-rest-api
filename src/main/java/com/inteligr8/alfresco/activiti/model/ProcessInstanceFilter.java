

package com.inteligr8.alfresco.activiti.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.inteligr8.activiti.model.Datum;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "processDefinitionId",
    "processDefinitionKey",
    "sort",
    "asc",
    "state"
})
public class ProcessInstanceFilter extends Datum {
    
    public enum State {
        @JsonProperty("all")
        All,
        @JsonProperty("completed")
        Completed,
        @JsonProperty("running")
        Running
    }

    @JsonProperty("name")
    private String name;
    @JsonProperty("processDefinitionId")
    private String processDefinitionId;
    @JsonProperty("processDefinitionKey")
    private String processDefinitionKey;
    @JsonProperty("sort")
    private String sort;
    @JsonProperty("asc")
    private Boolean asc;
    @JsonProperty("state")
    private State state;

    /**
     * No args constructor for use in serialization
     */
    public ProcessInstanceFilter() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProcessInstanceFilter withName(String name) {
        this.name = name;
        return this;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String id) {
        this.processDefinitionId = id;
    }

    public ProcessInstanceFilter withProcessDefinitionId(String id) {
        this.processDefinitionId = id;
        return this;
    }
    
    public String getProcessDefinitionKey() {
        return processDefinitionKey;
    }
    
    public void setProcessDefinitionKey(String key) {
        this.processDefinitionKey = key;
    }
    
    public ProcessInstanceFilter withProcessDefinitionKey(String key) {
        this.processDefinitionKey = key;
        return this;
    }
    
    public String getSort() {
        return sort;
    }
    
    public void setSort(String sort) {
        this.sort = sort;
    }
    
    public ProcessInstanceFilter withSort(String sort) {
        this.sort = sort;
        return this;
    }
    
    public Boolean isAsc() {
        return asc;
    }
    
    public void setAsc(Boolean asc) {
        this.asc = asc;
    }
    
    public ProcessInstanceFilter withAsc(Boolean asc) {
        this.asc = asc;
        return this;
    }
    
    public State getState() {
        return state;
    }
    
    public void setState(State state) {
        this.state = state;
    }
    
    public ProcessInstanceFilter withState(State state) {
        this.state = state;
        return this;
    }

}
