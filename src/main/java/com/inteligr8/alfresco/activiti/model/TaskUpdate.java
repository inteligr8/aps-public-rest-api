
package com.inteligr8.alfresco.activiti.model;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "category",
    "categorySet",
    "description",
    "descriptionSet",
    "dueDate",
    "dueDateSet",
    "formKey",
    "formKeySet",
    "name",
    "nameSet",
    "parentTaskId",
    "parentTaskIdSet",
    "priority",
    "prioritySet"
})
public class TaskUpdate {

    @JsonProperty("category")
    private String category;
    @JsonProperty("categorySet")
    private Boolean categorySet;
    @JsonProperty("description")
    private String description;
    @JsonProperty("descriptionSet")
    private Boolean descriptionSet;
    @JsonProperty("dueDate")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime dueDate;
    @JsonProperty("dueDateSet")
    private Boolean dueDateSet;
    @JsonProperty("formKey")
    private String formKey;
    @JsonProperty("formKeySet")
    private Boolean formKeySet;
    @JsonProperty("name")
    private String name;
    @JsonProperty("nameSet")
    private Boolean nameSet;
    @JsonProperty("parentTaskId")
    private String parentTaskId;
    @JsonProperty("parentTaskIdSet")
    private Boolean parentTaskIdSet;
    @JsonProperty("priority")
    private Long priority;
    @JsonProperty("prioritySet")
    private Boolean prioritySet;

    /**
     * No args constructor for use in serialization
     */
    public TaskUpdate() {
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public TaskUpdate withCategory(String category) {
        this.category = category;
        return this;
    }

    public Boolean getCategorySet() {
        return categorySet;
    }

    public void setCategorySet(Boolean categorySet) {
        this.categorySet = categorySet;
    }

    public TaskUpdate withCategorySet(Boolean categorySet) {
        this.categorySet = categorySet;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TaskUpdate withDescription(String description) {
        this.description = description;
        return this;
    }

    public Boolean getDescriptionSet() {
        return descriptionSet;
    }

    public void setDescriptionSet(Boolean descriptionSet) {
        this.descriptionSet = descriptionSet;
    }

    public TaskUpdate withDescriptionSet(Boolean descriptionSet) {
        this.descriptionSet = descriptionSet;
        return this;
    }

    public OffsetDateTime getDueDate() {
        return dueDate;
    }

    public void setDueDate(OffsetDateTime dueDate) {
        this.dueDate = dueDate;
    }

    public TaskUpdate withDueDate(OffsetDateTime dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    public Boolean getDueDateSet() {
        return dueDateSet;
    }

    public void setDueDateSet(Boolean dueDateSet) {
        this.dueDateSet = dueDateSet;
    }

    public TaskUpdate withDueDateSet(Boolean dueDateSet) {
        this.dueDateSet = dueDateSet;
        return this;
    }

    public String getFormKey() {
        return formKey;
    }

    public void setFormKey(String formKey) {
        this.formKey = formKey;
    }

    public TaskUpdate withFormKey(String formKey) {
        this.formKey = formKey;
        return this;
    }

    public Boolean getFormKeySet() {
        return formKeySet;
    }

    public void setFormKeySet(Boolean formKeySet) {
        this.formKeySet = formKeySet;
    }

    public TaskUpdate withFormKeySet(Boolean formKeySet) {
        this.formKeySet = formKeySet;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TaskUpdate withName(String name) {
        this.name = name;
        return this;
    }

    public Boolean getNameSet() {
        return nameSet;
    }

    public void setNameSet(Boolean nameSet) {
        this.nameSet = nameSet;
    }

    public TaskUpdate withNameSet(Boolean nameSet) {
        this.nameSet = nameSet;
        return this;
    }

    public String getParentTaskId() {
        return parentTaskId;
    }

    public void setParentTaskId(String parentTaskId) {
        this.parentTaskId = parentTaskId;
    }

    public TaskUpdate withParentTaskId(String parentTaskId) {
        this.parentTaskId = parentTaskId;
        return this;
    }

    public Boolean getParentTaskIdSet() {
        return parentTaskIdSet;
    }

    public void setParentTaskIdSet(Boolean parentTaskIdSet) {
        this.parentTaskIdSet = parentTaskIdSet;
    }

    public TaskUpdate withParentTaskIdSet(Boolean parentTaskIdSet) {
        this.parentTaskIdSet = parentTaskIdSet;
        return this;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    public TaskUpdate withPriority(Long priority) {
        this.priority = priority;
        return this;
    }

    public Boolean getPrioritySet() {
        return prioritySet;
    }

    public void setPrioritySet(Boolean prioritySet) {
        this.prioritySet = prioritySet;
    }

    public TaskUpdate withPrioritySet(Boolean prioritySet) {
        this.prioritySet = prioritySet;
        return this;
    }
    
}
