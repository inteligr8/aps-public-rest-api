

package com.inteligr8.alfresco.activiti.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "filterId",
    "filter",
    "appDefinitionId",
    "page",
    "size"
})
public class TaskFilterRepresentation {

    @JsonProperty("filterId")
    private Long filterId;
    @JsonProperty("filter")
    private TaskFilter filter;
    @JsonProperty("appDefinitionId")
    private Long appDefinitionId;
    @JsonProperty("page")
    private Integer page;
    @JsonProperty("size")
    private Integer size;

    /**
     * No args constructor for use in serialization
     */
    public TaskFilterRepresentation() {
    }
    
    public Long getFilterId() {
        return filterId;
    }
    
    public void setFilterId(Long filterId) {
        this.filterId = filterId;
    }
    
    public TaskFilterRepresentation withFilterId(Long filterId) {
        this.filterId = filterId;
        return this;
    }
    
    public TaskFilter getFilter() {
        return filter;
    }
    
    public void setFilter(TaskFilter filter) {
        this.filter = filter;
    }
    
    public TaskFilterRepresentation withFilter(TaskFilter filter) {
        this.filter = filter;
        return this;
    }

    public Long getAppDefinitionId() {
        return appDefinitionId;
    }

    public void setAppDefinitionId(Long appDefinitionId) {
        this.appDefinitionId = appDefinitionId;
    }

    public TaskFilterRepresentation withAppDefinitionId(Long appDefinitionId) {
        this.appDefinitionId = appDefinitionId;
        return this;
    }
    
    public Integer getPage() {
        return page;
    }
    
    public void setPage(Integer page) {
        this.page = page;
    }
    
    public TaskFilterRepresentation withPage(Integer page) {
        this.page = page;
        return this;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public TaskFilterRepresentation withSize(Integer size) {
        this.size = size;
        return this;
    }

}
