package com.inteligr8.alfresco.activiti.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum PermissionLevel {
	
	@JsonProperty("write")
	Write,

	@JsonProperty("read")
	Read

}
