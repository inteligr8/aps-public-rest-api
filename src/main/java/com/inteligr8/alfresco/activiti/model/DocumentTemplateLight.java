package com.inteligr8.alfresco.activiti.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DocumentTemplateLight extends BaseTemplateLight {

    @JsonProperty
    private Integer modelId;
	@JsonProperty
	private String mimeType;
    @JsonProperty
    private String simpleType;
    
    public Integer getModelId() {
        return modelId;
    }
    
    public void setModelId(Integer modelId) {
        this.modelId = modelId;
    }
    
    public String getMimeType() {
        return mimeType;
    }
    
    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
    
    public String getSimpleType() {
        return simpleType;
    }
    
    public void setSimpleType(String simpleType) {
        this.simpleType = simpleType;
    }

}
