

package com.inteligr8.alfresco.activiti.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "businessKey",
    "name",
    "outcome",
    "processDefinitionId",
    "processDefinitionKey",
    "values",
    "variables"
})
public class CreateProcessInstance {

    @JsonProperty("businessKey")
    private String businessKey;
    @JsonProperty("name")
    private String name;
    @JsonProperty("outcome")
    private String outcome;
    @JsonProperty("processDefinitionId")
    private String processDefinitionId;
    @JsonProperty("processDefinitionKey")
    private String processDefinitionKey;
    @JsonProperty("values")
    private Object values;
    @JsonProperty("variables")
    private List<Variable> variables = new ArrayList<Variable>();

    /**
     * No args constructor for use in serialization
     */
    public CreateProcessInstance() {
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public CreateProcessInstance withBusinessKey(String businessKey) {
        this.businessKey = businessKey;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CreateProcessInstance withName(String name) {
        this.name = name;
        return this;
    }

    public String getOutcome() {
        return outcome;
    }

    public void setOutcome(String outcome) {
        this.outcome = outcome;
    }

    public CreateProcessInstance withOutcome(String outcome) {
        this.outcome = outcome;
        return this;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public CreateProcessInstance withProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
        return this;
    }

    public String getProcessDefinitionKey() {
        return processDefinitionKey;
    }

    public void setProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
    }

    public CreateProcessInstance withProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
        return this;
    }

    public Object getValues() {
        return values;
    }

    public void setValues(Object values) {
        this.values = values;
    }

    public CreateProcessInstance withValues(Object values) {
        this.values = values;
        return this;
    }

    public List<Variable> getVariables() {
        return variables;
    }

    public void setVariables(List<Variable> variables) {
        this.variables = variables;
    }

    public CreateProcessInstance withVariables(List<Variable> variables) {
        this.variables = variables;
        return this;
    }

}
