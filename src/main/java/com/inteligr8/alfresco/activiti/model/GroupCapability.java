
package com.inteligr8.alfresco.activiti.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name"
})
public class GroupCapability {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("name")
    private String name;

    /**
     * No args constructor for use in serialization
     */
    public GroupCapability() {
    }

    public GroupCapability(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GroupCapability withId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GroupCapability withName(String name) {
        this.name = name;
        return this;
    }

}
