

package com.inteligr8.alfresco.activiti.model;

import java.time.OffsetDateTime;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonFormat.Shape;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "excludeSubprocesses",
    "finished",
    "finishedAfter",
    "finishedBefore",
    "includeProcessVariables",
    "involvedUser",
    "order",
    "processBusinessKey",
    "processDefinitionId",
    "processDefinitionKey",
    "processInstanceId",
    "processInstanceIds",
    "size",
    "sort",
    "start",
    "startedAfter",
    "startedBefore",
    "startedBy",
    "superProcessInstanceId",
    "tenantId",
    "tenantIdLike",
    "variables",
    "withoutTenantId"
})
public class HistoricProcessInstanceQueryRepresentation {

    @JsonProperty("excludeSubprocesses")
    private Boolean excludeSubprocesses;
    @JsonProperty("finished")
    private Boolean finished;
    @JsonProperty("finishedAfter")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime finishedAfter;
    @JsonProperty("finishedBefore")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime finishedBefore;
    @JsonProperty("includeProcessVariables")
    private Boolean includeProcessVariables;
    @JsonProperty("involvedUser")
    private String involvedUser;
    @JsonProperty("order")
    private String order;
    @JsonProperty("processBusinessKey")
    private String processBusinessKey;
    @JsonProperty("processDefinitionId")
    private String processDefinitionId;
    @JsonProperty("processDefinitionKey")
    private String processDefinitionKey;
    @JsonProperty("processInstanceId")
    private String processInstanceId;
    @JsonProperty("processInstanceIds")
    private List<String> processInstanceIds;
    @JsonProperty("size")
    private Integer size;
    @JsonProperty("sort")
    private String sort;
    @JsonProperty("start")
    private Integer start;
    @JsonProperty("startedAfter")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime startedAfter;
    @JsonProperty("startedBefore")
    @JsonFormat(shape = Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXX")
    private OffsetDateTime startedBefore;
    @JsonProperty("startedBy")
    private String startedBy;
    @JsonProperty("superProcessInstanceId")
    private String superProcessInstanceId;
    @JsonProperty("tenantId")
    private String tenantId;
    @JsonProperty("tenantIdLike")
    private String tenantIdLike;
    @JsonProperty("variables")
    private List<QueryVariable> variables;
    @JsonProperty("withoutTenantId")
    private Boolean withoutTenantId;

    /**
     * No args constructor for use in serialization
     */
    public HistoricProcessInstanceQueryRepresentation() {
    }

    public Boolean isExcludeSubprocesses() {
        return excludeSubprocesses;
    }

    public void setExcludeSubprocesses(Boolean excludeSubprocesses) {
        this.excludeSubprocesses = excludeSubprocesses;
    }

    public HistoricProcessInstanceQueryRepresentation withExcludeSubprocesses(Boolean excludeSubprocesses) {
        this.excludeSubprocesses = excludeSubprocesses;
        return this;
    }

    public Boolean isFinished() {
        return finished;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }

    public HistoricProcessInstanceQueryRepresentation withFinished(Boolean finished) {
        this.finished = finished;
        return this;
    }

    public OffsetDateTime getFinishedAfter() {
        return finishedAfter;
    }

    public void setFinishedAfter(OffsetDateTime finishedAfter) {
        this.finishedAfter = finishedAfter;
    }

    public HistoricProcessInstanceQueryRepresentation withFinishedAfter(OffsetDateTime finishedAfter) {
        this.finishedAfter = finishedAfter;
        return this;
    }

    public OffsetDateTime getFinishedBefore() {
        return finishedBefore;
    }

    public void setFinishedBefore(OffsetDateTime finishedBefore) {
        this.finishedBefore = finishedBefore;
    }

    public HistoricProcessInstanceQueryRepresentation withFinishedBefore(OffsetDateTime finishedBefore) {
        this.finishedBefore = finishedBefore;
        return this;
    }

    public Boolean isIncludeProcessVariables() {
        return includeProcessVariables;
    }

    public void setIncludeProcessVariables(Boolean includeProcessVariables) {
        this.includeProcessVariables = includeProcessVariables;
    }

    public HistoricProcessInstanceQueryRepresentation withIncludeProcessVariables(Boolean includeProcessVariables) {
        this.includeProcessVariables = includeProcessVariables;
        return this;
    }

    public String getInvolvedUser() {
        return involvedUser;
    }

    public void setInvolvedUser(String involvedUser) {
        this.involvedUser = involvedUser;
    }

    public HistoricProcessInstanceQueryRepresentation withInvolvedUser(String involvedUser) {
        this.involvedUser = involvedUser;
        return this;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public HistoricProcessInstanceQueryRepresentation withOrder(String order) {
        this.order = order;
        return this;
    }

    public String getProcessBusinessKey() {
        return processBusinessKey;
    }

    public void setProcessBusinessKey(String processBusinessKey) {
        this.processBusinessKey = processBusinessKey;
    }

    public HistoricProcessInstanceQueryRepresentation withProcessBusinessKey(String processBusinessKey) {
        this.processBusinessKey = processBusinessKey;
        return this;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public HistoricProcessInstanceQueryRepresentation withProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
        return this;
    }

    public String getProcessDefinitionKey() {
        return processDefinitionKey;
    }

    public void setProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
    }

    public HistoricProcessInstanceQueryRepresentation withProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
        return this;
    }

    public String getProcessInstanceId() {
        return processInstanceId;
    }

    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }

    public HistoricProcessInstanceQueryRepresentation withProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
        return this;
    }

    public List<String> getProcessInstanceIds() {
        return processInstanceIds;
    }

    public void setProcessInstanceIds(List<String> processInstanceIds) {
        this.processInstanceIds = processInstanceIds;
    }

    public HistoricProcessInstanceQueryRepresentation withProcessInstanceIds(List<String> processInstanceIds) {
        this.processInstanceIds = processInstanceIds;
        return this;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public HistoricProcessInstanceQueryRepresentation withSize(Integer size) {
        this.size = size;
        return this;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public HistoricProcessInstanceQueryRepresentation withSort(String sort) {
        this.sort = sort;
        return this;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public HistoricProcessInstanceQueryRepresentation withStart(Integer start) {
        this.start = start;
        return this;
    }

    public OffsetDateTime getStartedAfter() {
        return startedAfter;
    }

    public void setStartedAfter(OffsetDateTime startedAfter) {
        this.startedAfter = startedAfter;
    }

    public HistoricProcessInstanceQueryRepresentation withStartedAfter(OffsetDateTime startedAfter) {
        this.startedAfter = startedAfter;
        return this;
    }

    public OffsetDateTime getStartedBefore() {
        return startedBefore;
    }

    public void setStartedBefore(OffsetDateTime startedBefore) {
        this.startedBefore = startedBefore;
    }

    public HistoricProcessInstanceQueryRepresentation withStartedBefore(OffsetDateTime startedBefore) {
        this.startedBefore = startedBefore;
        return this;
    }

    public String getStartedBy() {
        return startedBy;
    }

    public void setStartedBy(String startedBy) {
        this.startedBy = startedBy;
    }

    public HistoricProcessInstanceQueryRepresentation withStartedBy(String startedBy) {
        this.startedBy = startedBy;
        return this;
    }

    public String getSuperProcessInstanceId() {
        return superProcessInstanceId;
    }

    public void setSuperProcessInstanceId(String superProcessInstanceId) {
        this.superProcessInstanceId = superProcessInstanceId;
    }

    public HistoricProcessInstanceQueryRepresentation withSuperProcessInstanceId(String superProcessInstanceId) {
        this.superProcessInstanceId = superProcessInstanceId;
        return this;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public HistoricProcessInstanceQueryRepresentation withTenantId(String tenantId) {
        this.tenantId = tenantId;
        return this;
    }

    public String getTenantIdLike() {
        return tenantIdLike;
    }

    public void setTenantIdLike(String tenantIdLike) {
        this.tenantIdLike = tenantIdLike;
    }

    public HistoricProcessInstanceQueryRepresentation withTenantIdLike(String tenantIdLike) {
        this.tenantIdLike = tenantIdLike;
        return this;
    }

    public List<QueryVariable> getVariables() {
    	if (variables == null)
    		variables = new LinkedList<>();
        return variables;
    }

    public void setVariables(List<QueryVariable> variables) {
        this.variables = variables;
    }

    public HistoricProcessInstanceQueryRepresentation withVariables(List<QueryVariable> variables) {
        this.variables = variables;
        return this;
    }

    public Boolean isWithoutTenantId() {
        return withoutTenantId;
    }

    public void setWithoutTenantId(Boolean withoutTenantId) {
        this.withoutTenantId = withoutTenantId;
    }

    public HistoricProcessInstanceQueryRepresentation withWithoutTenantId(Boolean withoutTenantId) {
        this.withoutTenantId = withoutTenantId;
        return this;
    }
    
    

}
