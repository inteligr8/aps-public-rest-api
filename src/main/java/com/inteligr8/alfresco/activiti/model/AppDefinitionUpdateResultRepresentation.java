
package com.inteligr8.alfresco.activiti.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * AppDefinitionUpdateResultRepresentation
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "appDefinition",
    "customData",
    "error",
    "errorDescription",
    "errorType",
    "message",
    "messageKey"
})
public class AppDefinitionUpdateResultRepresentation {

    /**
     * AppDefinitionRepresentation
     * <p>
     * 
     * 
     */
    @JsonProperty("appDefinition")
    private AppDefinition appDefinition;
    @JsonProperty("customData")
    private CustomData customData;
    @JsonProperty("error")
    private Boolean error;
    @JsonProperty("errorDescription")
    private String errorDescription;
    @JsonProperty("errorType")
    private Integer errorType;
    @JsonProperty("message")
    private String message;
    @JsonProperty("messageKey")
    private String messageKey;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * AppDefinitionRepresentation
     * <p>
     * 
     * 
     */
    @JsonProperty("appDefinition")
    public AppDefinition getAppDefinition() {
        return appDefinition;
    }

    /**
     * AppDefinitionRepresentation
     * <p>
     * 
     * 
     */
    @JsonProperty("appDefinition")
    public void setAppDefinition(AppDefinition appDefinition) {
        this.appDefinition = appDefinition;
    }

    public AppDefinitionUpdateResultRepresentation withAppDefinition(AppDefinition appDefinition) {
        this.appDefinition = appDefinition;
        return this;
    }

    @JsonProperty("customData")
    public CustomData getCustomData() {
        return customData;
    }

    @JsonProperty("customData")
    public void setCustomData(CustomData customData) {
        this.customData = customData;
    }

    public AppDefinitionUpdateResultRepresentation withCustomData(CustomData customData) {
        this.customData = customData;
        return this;
    }

    @JsonProperty("error")
    public Boolean getError() {
        return error;
    }

    @JsonProperty("error")
    public void setError(Boolean error) {
        this.error = error;
    }

    public AppDefinitionUpdateResultRepresentation withError(Boolean error) {
        this.error = error;
        return this;
    }

    @JsonProperty("errorDescription")
    public String getErrorDescription() {
        return errorDescription;
    }

    @JsonProperty("errorDescription")
    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

    public AppDefinitionUpdateResultRepresentation withErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
        return this;
    }

    @JsonProperty("errorType")
    public Integer getErrorType() {
        return errorType;
    }

    @JsonProperty("errorType")
    public void setErrorType(Integer errorType) {
        this.errorType = errorType;
    }

    public AppDefinitionUpdateResultRepresentation withErrorType(Integer errorType) {
        this.errorType = errorType;
        return this;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

    public AppDefinitionUpdateResultRepresentation withMessage(String message) {
        this.message = message;
        return this;
    }

    @JsonProperty("messageKey")
    public String getMessageKey() {
        return messageKey;
    }

    @JsonProperty("messageKey")
    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    public AppDefinitionUpdateResultRepresentation withMessageKey(String messageKey) {
        this.messageKey = messageKey;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public AppDefinitionUpdateResultRepresentation withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}
