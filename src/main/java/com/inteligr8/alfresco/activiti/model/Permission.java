package com.inteligr8.alfresco.activiti.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Permission extends PermissionLight {
	
	@JsonProperty
	private UserLight person;
	@JsonProperty
	private GroupLight group;

	public UserLight getPerson() {
		return person;
	}

	public void setPerson(UserLight person) {
		this.person = person;
	}

	public GroupLight getGroup() {
		return group;
	}

	public void setGroup(GroupLight group) {
		this.group = group;
	}
	

}
