package com.inteligr8.alfresco.activiti.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssigneeIdentifier {
	
	@JsonProperty
	private String assignee;
	@JsonProperty
	private String email;
	
    public String getAssignee() {
        return assignee;
    }
    
    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

	public AssigneeIdentifier withAssignee(String assignee) {
        this.assignee = assignee;
        return this;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }

	public AssigneeIdentifier withEmail(String email) {
        this.email = email;
        return this;
    }

}
