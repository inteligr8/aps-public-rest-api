package com.inteligr8.alfresco.activiti.model;

import java.io.InputStream;
import java.text.ParseException;

import org.glassfish.jersey.media.multipart.BodyPart;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;

public class FileMultipartJersey extends FormDataMultiPart {
	
	public static FileMultipartJersey from(String filename, InputStream istream) throws ParseException {
		FileMultipartJersey multipart = new FileMultipartJersey();
		multipart.bodyPart(toBodyPart(filename, istream));
		return multipart;
	}
	
	private FileMultipartJersey() {
	}
	
	public FormDataBodyPart getFileAttachment() {
		return this.getField("file");
	}

	private static BodyPart toBodyPart(String filename, InputStream istream) throws ParseException {
		if (filename == null) {
			return new FormDataBodyPart()
					.contentDisposition(new FormDataContentDisposition("form-data; name=\"file\""))
					.entity(istream);
		} else {
			return new FormDataBodyPart()
					.contentDisposition(new FormDataContentDisposition("form-data; name=\"file\"; filename=\"" + filename + "\""))
					.entity(istream);
		}
	}

}
