package com.inteligr8.alfresco.activiti.model;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Variable extends com.inteligr8.activiti.model.Variable {

    /**
     * No args constructor for use in serialization
     */
    public Variable() {
    }

    public Variable(String name, String scope, String type, Object value) {
        super(name, scope, type, value);
    }
    
}
