package com.inteligr8.alfresco.activiti.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GroupLight {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("externalId")
    private String externalId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("parentGroupId")
    private Long parentGroupId;
    @JsonProperty("status")
    private String status;

    /**
     * No args constructor for use in serialization
     */
    public GroupLight() {
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public GroupLight withId(Long id) {
		this.setId(id);
		return this;
	}

	public String getExternalId() {
		return externalId;
	}

	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
	
	public GroupLight withExternalId(String externalId) {
		this.setExternalId(externalId);
		return this;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public GroupLight withName(String name) {
		this.setName(name);
		return this;
	}

	public Long getParentGroupId() {
		return parentGroupId;
	}

	public void setParentGroupId(Long parentGroupId) {
		this.parentGroupId = parentGroupId;
	}
	
	public GroupLight withParentGroupId(Long parentGroupId) {
		this.setParentGroupId(parentGroupId);
		return this;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public GroupLight withStatus(String status) {
		this.setStatus(status);
		return this;
	}
    
}

