
package com.inteligr8.alfresco.activiti.model;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * AppDefinitionRepresentation
 * <p>
 * 
 * 
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "defaultAppId",
    "deploymentId",
    "description",
    "icon",
    "id",
    "modelId",
    "name",
    "tenantId",
    "theme"
})
public class AppDefinition {

    @JsonProperty("defaultAppId")
    private String defaultAppId;
    @JsonProperty("deploymentId")
    private String deploymentId;
    @JsonProperty("description")
    private String description;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("id")
    private Long id;
    @JsonProperty("modelId")
    private Long modelId;
    @JsonProperty("name")
    private String name;
    @JsonProperty("tenantId")
    private Long tenantId;
    @JsonProperty("theme")
    private String theme;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("defaultAppId")
    public String getDefaultAppId() {
        return defaultAppId;
    }

    @JsonProperty("defaultAppId")
    public void setDefaultAppId(String defaultAppId) {
        this.defaultAppId = defaultAppId;
    }

    public AppDefinition withDefaultAppId(String defaultAppId) {
        this.defaultAppId = defaultAppId;
        return this;
    }

    @JsonProperty("deploymentId")
    public String getDeploymentId() {
        return deploymentId;
    }

    @JsonProperty("deploymentId")
    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }

    public AppDefinition withDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
        return this;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    public AppDefinition withDescription(String description) {
        this.description = description;
        return this;
    }

    @JsonProperty("icon")
    public String getIcon() {
        return icon;
    }

    @JsonProperty("icon")
    public void setIcon(String icon) {
        this.icon = icon;
    }

    public AppDefinition withIcon(String icon) {
        this.icon = icon;
        return this;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    public AppDefinition withId(Long id) {
        this.id = id;
        return this;
    }

    @JsonProperty("modelId")
    public Long getModelId() {
        return modelId;
    }

    @JsonProperty("modelId")
    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    public AppDefinition withModelId(Long modelId) {
        this.modelId = modelId;
        return this;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    public AppDefinition withName(String name) {
        this.name = name;
        return this;
    }

    @JsonProperty("tenantId")
    public Long getTenantId() {
        return tenantId;
    }

    @JsonProperty("tenantId")
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    public AppDefinition withTenantId(Long tenantId) {
        this.tenantId = tenantId;
        return this;
    }

    @JsonProperty("theme")
    public String getTheme() {
        return theme;
    }

    @JsonProperty("theme")
    public void setTheme(String theme) {
        this.theme = theme;
    }

    public AppDefinition withTheme(String theme) {
        this.theme = theme;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public AppDefinition withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

}
