package com.inteligr8.alfresco.activiti;

import com.inteligr8.alfresco.activiti.api.AppDefinitionsJerseyApi;
import com.inteligr8.alfresco.activiti.api.TemplatesJerseyApi;

/**
 * This interface appends Jersey implementation specific methods to the JAX-RS
 * API of the APS Public ReST API.  This is due to a lack of multi-part in the
 * JAX-RS specification.
 * 
 * @author brian@inteligr8.com
 */
public interface ApsPublicRestJerseyApi extends ApsPublicRestApi {
	
	default AppDefinitionsJerseyApi getAppDefinitionsJerseyApi() {
		return this.getApi(AppDefinitionsJerseyApi.class);
	}
    
    default TemplatesJerseyApi getTemplatesJerseyApi() {
        return this.getApi(TemplatesJerseyApi.class);
    }

}
