/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.activiti;

import com.inteligr8.alfresco.activiti.api.ShareApi;
import com.inteligr8.alfresco.activiti.api.TemplatesApi;

/**
 * This interface consolidates the JAX-RS APIs available in the Activiti & APS
 * public ReST APIs, plus some useful non-public APIs available in APS.
 * 
 * @author brian@inteligr8.com
 */
public interface ApsProtectedRestApi extends ApsPublicRestApi {
	
	default ShareApi getShareApi() {
		return this.getApi(ShareApi.class);
	}

    default TemplatesApi getTemplatesApi() {
        return this.getApi(TemplatesApi.class);
    }

}
