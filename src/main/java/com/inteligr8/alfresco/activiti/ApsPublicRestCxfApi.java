package com.inteligr8.alfresco.activiti;

import com.inteligr8.alfresco.activiti.api.AppDefinitionsCxfApi;
import com.inteligr8.alfresco.activiti.api.TemplatesCxfApi;

/**
 * This interface appends Apache CXF implementation specific methods to the
 * JAX-RS API of the APS Public ReST API.  This is due to a lack of multi-part
 * in the JAX-RS specification.
 * 
 * @author brian@inteligr8.com
 */
public interface ApsPublicRestCxfApi extends ApsPublicRestApi {
	
	default AppDefinitionsCxfApi getAppDefinitionsCxfApi() {
		return this.getApi(AppDefinitionsCxfApi.class);
	}
	
	default TemplatesCxfApi getTemplatesCxfApi() {
	    return this.getApi(TemplatesCxfApi.class);
	}

}
