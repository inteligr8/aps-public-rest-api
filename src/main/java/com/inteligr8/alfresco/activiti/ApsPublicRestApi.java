/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.alfresco.activiti;

import com.inteligr8.activiti.ActivitiPublicRestApi;
import com.inteligr8.alfresco.activiti.api.AdminApi;
import com.inteligr8.alfresco.activiti.api.AppDefinitionsApi;
import com.inteligr8.alfresco.activiti.api.AppDeploymentsApi;
import com.inteligr8.alfresco.activiti.api.AppVersionApi;
import com.inteligr8.alfresco.activiti.api.HistoricApi;
import com.inteligr8.alfresco.activiti.api.ModelsApi;
import com.inteligr8.alfresco.activiti.api.ProcessDefinitionsApi;
import com.inteligr8.alfresco.activiti.api.ProcessInstancesApi;
import com.inteligr8.alfresco.activiti.api.ProfileApi;
import com.inteligr8.alfresco.activiti.api.TasksApi;

/**
 * This interface consolidates the JAX-RS APIs available in the Activiti & APS
 * public ReST APIs.
 * 
 * @author brian@inteligr8.com
 */
public interface ApsPublicRestApi extends ActivitiPublicRestApi {
	
	default AdminApi getAdminApi() {
		return this.getApi(AdminApi.class);
	}
    
    default AppDeploymentsApi getAppDeploymentsApi() {
        return this.getApi(AppDeploymentsApi.class);
    }
	
	default AppDefinitionsApi getAppDefinitionsApi() {
		return this.getApi(AppDefinitionsApi.class);
	}
	
	default AppVersionApi getAppVersionApi() {
		return this.getApi(AppVersionApi.class);
	}
    
    default HistoricApi getHistoricApi() {
        return this.getApi(HistoricApi.class);
    }
	
	default ModelsApi getModelsApi() {
		return this.getApi(ModelsApi.class);
	}
    
    default ProcessDefinitionsApi getProcessDefinitionsApi() {
        return this.getApi(ProcessDefinitionsApi.class);
    }
	
	default ProcessInstancesApi getProcessInstancesApi() {
		return this.getApi(ProcessInstancesApi.class);
	}
	
	default ProfileApi getProfileApi() {
		return this.getApi(ProfileApi.class);
	}
	
	default TasksApi getTasksApi() {
		return this.getApi(TasksApi.class);
	}

}
