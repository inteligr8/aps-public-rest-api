
package com.inteligr8.activiti.model;

import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultList<T> extends Datum {

    @JsonProperty("data")
    private List<T> data = new LinkedList<T>();
    @JsonProperty("size")
    private Integer size;
    @JsonProperty("total")
    private Integer total;
    @JsonProperty("start")
    private Integer start;
    @JsonProperty("sort")
    private String sort;
    @JsonProperty("order")
    private String order;
    
    public List<T> getData() {
        return data;
    }
    
    public void setData(List<T> data) {
        this.data = data;
    }
    
    public Integer getSize() {
		return size;
	}
    
    public void setSize(Integer size) {
		this.size = size;
	}
    
    public Integer getTotal() {
		return total;
	}
    
    public void setTotal(Integer total) {
		this.total = total;
	}

    public Integer getStart() {
		return start;
	}
    
    public void setStart(Integer start) {
		this.start = start;
	}
    
    public String getSort() {
        return sort;
    }
    
    public void setSort(String sort) {
        this.sort = sort;
    }
    
    public String getOrder() {
        return order;
    }
    
    public void setOrder(String order) {
        this.order = order;
    }

}
