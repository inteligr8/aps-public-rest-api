
package com.inteligr8.activiti.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProcessInstance extends Datum {

    @JsonProperty("activityId")
    private String activityId;
    @JsonProperty("businessKey")
    private String businessKey;
    @JsonProperty("completed")
    private Boolean completed;
    @JsonProperty("ended")
    private Boolean ended;
    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("processDefinitionId")
    private String processDefinitionId;
    @JsonProperty("processDefinitionKey")
    private String processDefinitionKey;
    @JsonProperty("processDefinitionUrl")
    private String processDefinitionUrl;
    @JsonProperty("suspended")
    private Boolean suspended;
    @JsonProperty("tenantId")
    private String tenantId;
    @JsonProperty("url")
    private String url;
    @JsonProperty("variables")
    private List<Variable> variables = new ArrayList<Variable>();

    /**
     * No args constructor for use in serialization
     */
    public ProcessInstance() {
    }
    
    public String getActivityId() {
        return activityId;
    }
    
    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }
    
    public ProcessInstance withActivityId(String activityId) {
        this.activityId = activityId;
        return this;
    }

    public String getBusinessKey() {
        return businessKey;
    }

    public void setBusinessKey(String businessKey) {
        this.businessKey = businessKey;
    }

    public ProcessInstance withBusinessKey(String businessKey) {
        this.businessKey = businessKey;
        return this;
    }
    
    public Boolean getCompleted() {
        return completed;
    }
    
    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }
    
    public ProcessInstance withCompleted(Boolean completed) {
        this.completed = completed;
        return this;
    }

    public Boolean getEnded() {
        return ended;
    }

    public void setEnded(Boolean ended) {
        this.ended = ended;
    }

    public ProcessInstance withEnded(Boolean ended) {
        this.ended = ended;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProcessInstance withId(String id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProcessInstance withName(String name) {
        this.name = name;
        return this;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public ProcessInstance withProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
        return this;
    }

    public String getProcessDefinitionKey() {
        return processDefinitionKey;
    }

    public void setProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
    }

    public ProcessInstance withProcessDefinitionKey(String processDefinitionKey) {
        this.processDefinitionKey = processDefinitionKey;
        return this;
    }

    public Boolean getSuspended() {
        return suspended;
    }

    public void setSuspended(Boolean suspended) {
        this.suspended = suspended;
    }

    public ProcessInstance withSuspended(Boolean suspended) {
        this.suspended = suspended;
        return this;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public ProcessInstance withTenantId(String tenantId) {
        this.tenantId = tenantId;
        return this;
    }
    
    public String getUrl() {
        return url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }
    
    public ProcessInstance withUrl(String url) {
        this.url = url;
        return this;
    }

    public List<Variable> getVariables() {
        return variables;
    }

    public void setVariables(List<Variable> variables) {
        this.variables = variables;
    }

    public ProcessInstance withVariables(List<Variable> variables) {
        this.variables = variables;
        return this;
    }

}
