package com.inteligr8.activiti.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SignalEventAction extends ProcessInstanceAction {
    
    @JsonProperty("signalName")
    private String signal;

    /**
     * No args constructor for use in serialization
     */
    public SignalEventAction() {
    }
    
    public SignalEventAction(ActionValue action, String signal) {
        super(action);
        this.signal = signal;
    }
    
    public String getSignal() {
        return signal;
    }
    
    public void setSignal(String signal) {
        this.signal = signal;
    }
    
    public SignalEventAction withSignal(String signal) {
        this.signal = signal;
        return this;
    }

}
