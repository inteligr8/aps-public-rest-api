

package com.inteligr8.activiti.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProcessDefinition extends Datum {

    @JsonProperty("category")
    private String category;
    @JsonProperty("deploymentId")
    private String deploymentId;
    @JsonProperty("deploymentUrl")
    private String deploymentUrl;
    @JsonProperty("diagramResource")
    private String diagramResource;
    @JsonProperty("description")
    private String description;
    @JsonProperty("graphicalNotationDefined")
    private boolean graphicalNotationDefined;
    @JsonProperty("id")
    private String id;
    @JsonProperty("key")
    private String key;
    @JsonProperty("name")
    private String name;
    @JsonProperty("resource")
    private String resource;
    @JsonProperty("startFormDefined")
    private boolean startFormDefined;
    @JsonProperty("suspended")
    private boolean suspended;
    @JsonProperty("tenantId")
    private String tenantId;
    @JsonProperty("url")
    private String url;
    @JsonProperty("version")
    private Integer version;

    /**
     * No args constructor for use in serialization
     */
    public ProcessDefinition() {
    }
    
    public String getCategory() {
        return category;
    }
    
    public void setCategory(String category) {
        this.category = category;
    }
    
    public ProcessDefinition withCategory(String category) {
        this.category = category;
        return this;
    }
    
    public String getDeploymentId() {
        return deploymentId;
    }
    
    public void setDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
    }
    
    public ProcessDefinition withDeploymentId(String deploymentId) {
        this.deploymentId = deploymentId;
        return this;
    }
    
    public String getDeploymentUrl() {
        return deploymentUrl;
    }
    
    public void setDeploymentUrl(String deploymentUrl) {
        this.deploymentUrl = deploymentUrl;
    }
    
    public ProcessDefinition withDeploymentUrl(String deploymentUrl) {
        this.deploymentUrl = deploymentUrl;
        return this;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public ProcessDefinition withDescription(String description) {
        this.description = description;
        return this;
    }
    
    public String getDiagramResource() {
        return diagramResource;
    }
    
    public void setDiagramResource(String diagramResource) {
        this.diagramResource = diagramResource;
    }
    
    public ProcessDefinition withDiagramResource(String diagramResource) {
        this.diagramResource = diagramResource;
        return this;
    }
    
    public boolean isGraphicalNotationDefined() {
        return graphicalNotationDefined;
    }
    
    public void setGraphicalNotationDefined(boolean graphicalNotationDefined) {
        this.graphicalNotationDefined = graphicalNotationDefined;
    }
    
    public ProcessDefinition withGraphicalNotationDefined(boolean graphicalNotationDefined) {
        this.graphicalNotationDefined = graphicalNotationDefined;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ProcessDefinition withId(String id) {
        this.id = id;
        return this;
    }
    
    public String getKey() {
        return key;
    }
    
    public void setKey(String key) {
        this.key = key;
    }
    
    public ProcessDefinition withKey(String key) {
        this.key = key;
        return this;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ProcessDefinition withName(String name) {
        this.name = name;
        return this;
    }
    
    public String getResource() {
        return resource;
    }
    
    public void setResource(String resource) {
        this.resource = resource;
    }
    
    public ProcessDefinition withResource(String resource) {
        this.resource = resource;
        return this;
    }
    
    public boolean isStartFormDefined() {
        return startFormDefined;
    }
    
    public void setStartFormDefined(boolean startFormDefined) {
        this.startFormDefined = startFormDefined;
    }
    
    public ProcessDefinition withStartFormDefined(boolean startFormDefined) {
        this.startFormDefined = startFormDefined;
        return this;
    }
    
    public boolean isSuspended() {
        return suspended;
    }
    
    public void setSuspended(boolean suspended) {
        this.suspended = suspended;
    }
    
    public ProcessDefinition withSuspended(boolean suspended) {
        this.suspended = suspended;
        return this;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public ProcessDefinition withTenantId(String tenantId) {
        this.tenantId = tenantId;
        return this;
    }
    
    public String getUrl() {
        return url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }
    
    public ProcessDefinition withUrl(String url) {
        this.url = url;
        return this;
    }
    
    public Integer getVersion() {
        return version;
    }
    
    public void setVersion(Integer version) {
        this.version = version;
    }
    
    public ProcessDefinition withVersion(Integer version) {
        this.version = version;
        return this;
    }

}
