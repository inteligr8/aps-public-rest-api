package com.inteligr8.activiti.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MessageEventAction extends ProcessInstanceAction {
    
    @JsonProperty("messageName")
    private String message;

    /**
     * No args constructor for use in serialization
     */
    public MessageEventAction() {
    }
    
    public MessageEventAction(ActionValue action, String message) {
        super(action);
        this.message = message;
    }
    
    public String getMessage() {
        return message;
    }
    
    public void setMessage(String message) {
        this.message = message;
    }

    public MessageEventAction withMessage(String message) {
        this.message = message;
        return this;
    }
    

}
