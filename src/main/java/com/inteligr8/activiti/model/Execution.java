
package com.inteligr8.activiti.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Execution extends Datum {

    @JsonProperty("activityId")
    private String activityId;
    @JsonProperty("id")
    private String id;
    @JsonProperty("parentId")
    private String parentId;
    @JsonProperty("parentUrl")
    private String parentUrl;
    @JsonProperty("processInstanceId")
    private String processInstanceId;
    @JsonProperty("processInstanceUrl")
    private String processInstanceUrl;
    @JsonProperty("superExecutionId")
    private String superExecutionId;
    @JsonProperty("superExecutionUrl")
    private String superExecutionUrl;
    @JsonProperty("suspended")
    private Boolean suspended;
    @JsonProperty("tenantId")
    private String tenantId;
    @JsonProperty("url")
    private String url;

    /**
     * No args constructor for use in serialization
     */
    public Execution() {
    }
    
    public String getActivityId() {
        return activityId;
    }
    
    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }
    
    public Execution withActivityId(String activityId) {
        this.activityId = activityId;
        return this;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Execution withId(String id) {
        this.id = id;
        return this;
    }
    
    public String getParentId() {
        return parentId;
    }
    
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
    
    public Execution withParentId(String parentId) {
        this.parentId = parentId;
        return this;
    }
    
    public String getParentUrl() {
        return parentUrl;
    }
    
    public void setParentUrl(String parentUrl) {
        this.parentUrl = parentUrl;
    }
    
    public Execution withParentUrl(String parentUrl) {
        this.parentUrl = parentUrl;
        return this;
    }
    
    public String getProcessInstanceId() {
        return processInstanceId;
    }
    
    public void setProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
    }
    
    public Execution withProcessInstanceId(String processInstanceId) {
        this.processInstanceId = processInstanceId;
        return this;
    }
    
    public String getProcessInstanceUrl() {
        return processInstanceUrl;
    }
    
    public void setProcessInstanceUrl(String processInstanceUrl) {
        this.processInstanceUrl = processInstanceUrl;
    }
    
    public Execution withProcessInstanceUrl(String processInstanceUrl) {
        this.processInstanceUrl = processInstanceUrl;
        return this;
    }
    
    public String getSuperExecutionId() {
        return superExecutionId;
    }
    
    public void setSuperExecutionId(String superExecutionId) {
        this.superExecutionId = superExecutionId;
    }
    
    public Execution withSuperExecutionId(String superExecutionId) {
        this.superExecutionId = superExecutionId;
        return this;
    }
    
    public String getSuperExecutionUrl() {
        return superExecutionUrl;
    }
    
    public void setSuperExecutionUrl(String superExecutionUrl) {
        this.superExecutionUrl = superExecutionUrl;
    }
    
    public Execution withSuperExecutionUrl(String superExecutionUrl) {
        this.superExecutionUrl = superExecutionUrl;
        return this;
    }

    public Boolean getSuspended() {
        return suspended;
    }

    public void setSuspended(Boolean suspended) {
        this.suspended = suspended;
    }

    public Execution withSuspended(Boolean suspended) {
        this.suspended = suspended;
        return this;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public Execution withTenantId(String tenantId) {
        this.tenantId = tenantId;
        return this;
    }
    
    public String getUrl() {
        return url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }
    
    public Execution withUrl(String url) {
        this.url = url;
        return this;
    }

}
