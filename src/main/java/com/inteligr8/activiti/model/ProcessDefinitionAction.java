package com.inteligr8.activiti.model;

import java.time.OffsetDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProcessDefinitionAction {

    public enum ActionValue {
        @JsonProperty("suspend")
        Suspend,
        @JsonProperty("activate")
        Activate
    }
    
    @JsonProperty("action")
    private ActionValue action;
    @JsonProperty("includeProcessInstances")
    private boolean includeProcessInstances;
    @JsonProperty("date")
    private OffsetDateTime date;
    @JsonProperty("category")
    private String category;

    /**
     * No args constructor for use in serialization
     */
    public ProcessDefinitionAction() {
    }
    
    public ProcessDefinitionAction(ActionValue action) {
        this.action = action;
    }

    public ActionValue getAction() {
        return action;
    }

    public void setAction(ActionValue action) {
        this.action = action;
    }

	public ProcessDefinitionAction withAction(ActionValue action) {
        this.action = action;
        return this;
    }
	
	public boolean isIncludeProcessInstances() {
        return includeProcessInstances;
    }
	
	public void setIncludeProcessInstances(boolean includeProcessInstances) {
        this.includeProcessInstances = includeProcessInstances;
    }
    
    public ProcessDefinitionAction withIncludeProcessInstances(boolean includeProcessInstances) {
        this.includeProcessInstances = includeProcessInstances;
        return this;
    }
	
	public OffsetDateTime getDate() {
        return date;
    }
	
	public void setDate(OffsetDateTime date) {
        this.date = date;
    }
    
    public ProcessDefinitionAction withDate(OffsetDateTime date) {
        this.date = date;
        return this;
    }
    
    public String getCategory() {
        return category;
    }
    
    public void setCategory(String category) {
        this.category = category;
    }
    
    public ProcessDefinitionAction withCategory(String category) {
        this.category = category;
        return this;
    }

}
