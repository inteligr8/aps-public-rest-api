package com.inteligr8.activiti.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EngineProperties {

    @JsonProperty("next.dbid")
    private String nextDbid;
    @JsonProperty("schema.history")
    private String schemaHistory;
    @JsonProperty("schema.version")
    private String schemaVersion;

    /**
     * No args constructor for use in serialization
     */
    public EngineProperties() {
    }

    public String getNextDbid() {
        return nextDbid;
    }

    public void setNextDbid(String nextDbid) {
        this.nextDbid = nextDbid;
    }

	public EngineProperties withNextDbid(String nextDbid) {
        this.nextDbid = nextDbid;
        return this;
    }

    public String getSchemaHistory() {
        return schemaHistory;
    }

    public void setSchemaHistory(String schemaHistory) {
        this.schemaHistory = schemaHistory;
    }

	public EngineProperties withSchemaHistory(String schemaHistory) {
        this.schemaHistory = schemaHistory;
        return this;
    }

    public String getSchemaVersion() {
        return schemaVersion;
    }

    public void setSchemaVersion(String schemaVersion) {
        this.schemaVersion = schemaVersion;
    }

	public EngineProperties withSchemaVersion(String schemaVersion) {
        this.schemaVersion = schemaVersion;
        return this;
    }

}
