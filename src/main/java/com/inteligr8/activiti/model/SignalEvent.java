package com.inteligr8.activiti.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SignalEvent {
    
    @JsonProperty("signalName")
    private String signal;
    @JsonProperty("tenantId")
    private String tenantId;
    @JsonProperty("async")
    private Boolean async;
    @JsonProperty("variables")
    private List<Variable> variables;

    /**
     * No args constructor for use in serialization
     */
    public SignalEvent() {
    }
    
    public SignalEvent(String signal, String tenantId, Boolean async) {
        this.signal = signal;
        this.tenantId = tenantId;
        this.async = async;
    }
    
    public String getSignal() {
        return signal;
    }
    
    public void setSignal(String signal) {
        this.signal = signal;
    }
    
    public SignalEvent withSignal(String signal) {
        this.signal = signal;
        return this;
    }
    
    public String getTenantId() {
        return tenantId;
    }
    
    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }
    
    public SignalEvent withTenantId(String tenantId) {
        this.tenantId = tenantId;
        return this;
    }
    
    public Boolean getAsync() {
        return async;
    }
    
    public void setAsync(Boolean async) {
        this.async = async;
    }
    
    public SignalEvent withAsync(Boolean async) {
        this.async = async;
        return this;
    }
    
    public List<Variable> getVariables() {
        return variables;
    }
    
    public void setVariables(List<Variable> variables) {
        this.variables = variables;
    }
    
    public SignalEvent withVariables(List<Variable> variables) {
        this.variables = variables;
        return this;
    }

}
