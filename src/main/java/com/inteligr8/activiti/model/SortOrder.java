package com.inteligr8.activiti.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum SortOrder {
    
    @JsonProperty("asc")
    Ascending,
    @JsonProperty("desc")
    Descending

}
