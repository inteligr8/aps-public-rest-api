package com.inteligr8.activiti.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProcessInstanceAction {

    public enum ActionValue {
        @JsonProperty("execute")
        Execute,
        @JsonProperty("suspend")
        Suspend,
        @JsonProperty("activate")
        Activate,
        @JsonProperty("signal")
        Signal,
        @JsonProperty("signalEventReceived")
        SignalReceived,
        @JsonProperty("messageEventReceived")
        MessageReceived
    }
    
    @JsonProperty("action")
    private ActionValue action;
    @JsonProperty("variables")
    private List<Variable> variables;

    /**
     * No args constructor for use in serialization
     */
    public ProcessInstanceAction() {
    }
    
    public ProcessInstanceAction(ActionValue action) {
        this.action = action;
    }

    public ActionValue getAction() {
        return action;
    }

    public void setAction(ActionValue action) {
        this.action = action;
    }

	public ProcessInstanceAction withAction(ActionValue action) {
        this.action = action;
        return this;
    }
	
	public List<Variable> getVariables() {
        return variables;
    }
	
	public void setVariables(List<Variable> variables) {
        this.variables = variables;
    }
    
    public ProcessInstanceAction withVariables(List<Variable> variables) {
        this.variables = variables;
        return this;
    }

}
