/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.activiti;

import com.inteligr8.activiti.api.DeadletterJobsApi;
import com.inteligr8.activiti.api.ExecutionApi;
import com.inteligr8.activiti.api.JobsApi;
import com.inteligr8.activiti.api.ManagementApi;
import com.inteligr8.activiti.api.ProcessDefinitionApi;
import com.inteligr8.activiti.api.ProcessInstanceApi;
import com.inteligr8.activiti.api.RuntimeApi;
import com.inteligr8.activiti.api.TimerJobsApi;

/**
 * This interface consolidates the JAX-RS APIs available in the open-source
 * Activiti Public ReST API.
 * 
 * @author brian@inteligr8.com
 */
public interface ActivitiPublicRestApi {
	
	<T> T getApi(Class<T> apiClass);
	
	default ManagementApi getManagementApi() {
		return this.getApi(ManagementApi.class);
	}
    
    default RuntimeApi getRuntimeApi() {
        return this.getApi(RuntimeApi.class);
    }
	
	default JobsApi getJobsApi() {
		return this.getApi(JobsApi.class);
	}
	
	default DeadletterJobsApi getDeadletterJobsApi() {
		return this.getApi(DeadletterJobsApi.class);
	}
	
	default TimerJobsApi getTimerJobsApi() {
		return this.getApi(TimerJobsApi.class);
	}
    
    default ProcessDefinitionApi getProcessDefinitionApi() {
        return this.getApi(ProcessDefinitionApi.class);
    }
    
    default ProcessInstanceApi getProcessInstanceApi() {
        return this.getApi(ProcessInstanceApi.class);
    }
    
    default ExecutionApi getExecutionApi() {
        return this.getApi(ExecutionApi.class);
    }
    
}
