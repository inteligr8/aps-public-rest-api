/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.activiti.api;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.inteligr8.activiti.model.Execution;
import com.inteligr8.activiti.model.ProcessInstanceAction;
import com.inteligr8.activiti.model.ResultList;
import com.inteligr8.activiti.model.SortOrder;
import com.inteligr8.activiti.model.Variable;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

@Path("/api/runtime/executions")
public interface ExecutionApi {
    
    public enum Sort {
        @JsonProperty("id")
        ProcessInstanceId,
        @JsonProperty("processDefinitionId")
        ProcessDefinitionId,
        @JsonProperty("tenantId")
        TenantId,
        @JsonProperty("processDefinitionKey")
        ProcessDefinitionKey;
    }
    
    public enum VariableScope {
        @JsonProperty("local")
        Local,
        @JsonProperty("global")
        Global,
    }

    @GET
    @Path("{executionId}")
    @Produces({ MediaType.APPLICATION_JSON })
    Execution get(
            @PathParam("executionId") String executionId);

    @GET
    @Path("{executionId}/activities")
    @Produces({ MediaType.APPLICATION_JSON })
    List<String> getActiveActivities(
            @PathParam("executionId") String executionId);

    @PUT
    @Path("{executionId}")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    Execution execute(
            @PathParam("executionId") String executionId,
            ProcessInstanceAction action);

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    ResultList<Execution> getByTenant(
            @QueryParam("id") String executionId,
            @QueryParam("activityId") String activityId,
            @QueryParam("processDefinitionKey") String processDefinitionKey,
            @QueryParam("processDefinitionId") String processDefinitionId,
            @QueryParam("processInstanceId") String processInstanceId,
            @QueryParam("messageEventSubscriptionName") String messageEventSubscriptionName,
            @QueryParam("signalEventSubscriptionName") String signalEventSubscriptionName,
            @QueryParam("parentId") String parentId,
            @QueryParam("tenantId") String tenantId,
            @QueryParam("sort") Sort sort,
            @QueryParam("order") SortOrder sortOrder,
            @QueryParam("start") Integer pageStart,
            @QueryParam("size") Integer pageSize);

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    ResultList<Execution> getByTenants(
            @QueryParam("id") String executionId,
            @QueryParam("activityId") String activityId,
            @QueryParam("processDefinitionKey") String processDefinitionKey,
            @QueryParam("processDefinitionId") String processDefinitionId,
            @QueryParam("processInstanceId") String processInstanceId,
            @QueryParam("messageEventSubscriptionName") String messageEventSubscriptionName,
            @QueryParam("signalEventSubscriptionName") String signalEventSubscriptionName,
            @QueryParam("parentId") String parentId,
            @QueryParam("tenantIdLike") String tenantIdLike,
            @QueryParam("sort") Sort sort,
            @QueryParam("order") SortOrder sortOrder,
            @QueryParam("start") Integer pageStart,
            @QueryParam("size") Integer pageSize);

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    ResultList<Execution> getByAny(
            @QueryParam("id") String executionId,
            @QueryParam("activityId") String activityId,
            @QueryParam("processDefinitionKey") String processDefinitionKey,
            @QueryParam("processDefinitionId") String processDefinitionId,
            @QueryParam("processInstanceId") String processInstanceId,
            @QueryParam("messageEventSubscriptionName") String messageEventSubscriptionName,
            @QueryParam("signalEventSubscriptionName") String signalEventSubscriptionName,
            @QueryParam("parentId") String parentId,
            @QueryParam("tenantId") String tenantId,
            @QueryParam("tenantIdLike") String tenantIdLike,
            @QueryParam("withoutTenantId") Boolean withoutTenantId,
            @QueryParam("sort") Sort sort,
            @QueryParam("order") SortOrder sortOrder,
            @QueryParam("start") Integer pageStart,
            @QueryParam("size") Integer pageSize);

    @GET
    @Path("{executionId}/variables")
    @Produces({ MediaType.APPLICATION_JSON })
    List<Variable> getVariables(
            @PathParam("executionId") String executionId,
            @QueryParam("scope") VariableScope scope);

    @GET
    @Path("{executionId}/variables/{variableName}")
    @Produces({ MediaType.APPLICATION_JSON })
    Variable getVariable(
            @PathParam("executionId") String executionId,
            @PathParam("variableName") String variableName);

    @POST
    @Path("{executionId}/variables")
    @Consumes({ MediaType.APPLICATION_JSON })
    void createVariables(
            @PathParam("executionId") String executionId,
            List<Variable> variables);

    @PUT
    @Path("{executionId}/variables")
    @Consumes({ MediaType.APPLICATION_JSON })
    void updateVariables(
            @PathParam("executionId") String executionId,
            List<Variable> variables);
    
    @PUT
    @Path("{executionId}/variables/{variableName}")
    @Consumes({ MediaType.APPLICATION_JSON })
    void updateVariable(
            @PathParam("executionId") String executionId,
            @PathParam("variableName") String variableName,
            Variable variable); 

}
