/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.activiti.api;

import java.io.File;
import java.time.LocalDate;

import com.inteligr8.activiti.model.Job;
import com.inteligr8.activiti.model.ProcessInstanceAction;
import com.inteligr8.activiti.model.ResultList;
import com.inteligr8.activiti.model.SortOrder;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

public interface BaseJobsApi {
    
    @GET
    @Path("{jobId}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Job getJob(
        @PathParam("jobId") String jobId);

    @DELETE
    @Path("{jobId}")
    public void deleteJob(
        @PathParam("jobId") String jobId);

    @POST
    @Path("{jobId}")
    @Consumes({ MediaType.APPLICATION_JSON })
    public void executeJob(
        @PathParam("jobId") String jobId,
        ProcessInstanceAction execute);

    @GET
    @Path("{jobId}/execution-stacktrace")
    @Produces({ MediaType.APPLICATION_OCTET_STREAM })
    public File getJobStacktrace(
        @PathParam("jobId") String jobId);

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public ResultList<Job> queryJobs(
        @QueryParam("processInstanceId") String processInstanceId,
        @QueryParam("executionId") String executionId,
        @QueryParam("processDefinitionId") String processDefinitionId,
        @QueryParam("withRetriesLeft") Boolean withRetriesLeft,
        @QueryParam("executable") Boolean executable,
        @QueryParam("dueBefore") LocalDate dueBefore,
        @QueryParam("dueAfter") LocalDate dueAfter,
        @QueryParam("withoutTenantId") Boolean withoutTenatId,
        @QueryParam("sort") String sort,
        @QueryParam("order") SortOrder sortOrder,
        @QueryParam("start") Integer pageStart,
        @QueryParam("size") Integer pageSize);

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public ResultList<Job> queryJobs(
        @QueryParam("processInstanceId") String processInstanceId,
        @QueryParam("executionId") String executionId,
        @QueryParam("processDefinitionId") String processDefinitionId,
        @QueryParam("withRetriesLeft") Boolean withRetriesLeft,
        @QueryParam("executable") Boolean executable,
        @QueryParam("dueBefore") LocalDate dueBefore,
        @QueryParam("dueAfter") LocalDate dueAfter,
        @QueryParam("tenantId") String tenantId,
        @QueryParam("tenantIdLike") String tenantIdLike,
        @QueryParam("sort") String sort,
        @QueryParam("order") SortOrder sortOrder,
        @QueryParam("start") Integer pageStart,
        @QueryParam("size") Integer pageSize);

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public ResultList<Job> queryJobs(
        @QueryParam("processInstanceId") String processInstanceId,
        @QueryParam("executionId") String executionId,
        @QueryParam("processDefinitionId") String processDefinitionId,
        @QueryParam("withRetriesLeft") Boolean withRetriesLeft,
        @QueryParam("executable") Boolean executable,
        @QueryParam("withException") Boolean withException,
        @QueryParam("dueBefore") LocalDate dueBefore,
        @QueryParam("dueAfter") LocalDate dueAfter,
        @QueryParam("exceptionMessage") String exceptionMessage,
        @QueryParam("withoutTenantId") Boolean withoutTenatId,
        @QueryParam("sort") String sort,
        @QueryParam("order") SortOrder sortOrder,
        @QueryParam("start") Integer pageStart,
        @QueryParam("size") Integer pageSize);

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public ResultList<Job> queryJobs(
        @QueryParam("processInstanceId") String processInstanceId,
        @QueryParam("executionId") String executionId,
        @QueryParam("processDefinitionId") String processDefinitionId,
        @QueryParam("withRetriesLeft") Boolean withRetriesLeft,
        @QueryParam("executable") Boolean executable,
        @QueryParam("withException") Boolean withException,
        @QueryParam("dueBefore") LocalDate dueBefore,
        @QueryParam("dueAfter") LocalDate dueAfter,
        @QueryParam("exceptionMessage") String exceptionMessage,
        @QueryParam("tenantId") String tenantId,
        @QueryParam("tenantIdLike") String tenantIdLike,
        @QueryParam("sort") String sort,
        @QueryParam("order") SortOrder sortOrder,
        @QueryParam("start") Integer pageStart,
        @QueryParam("size") Integer pageSize);

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public ResultList<Job> queryJobs(
        @QueryParam("id") String jobId,
        @QueryParam("processInstanceId") String processInstanceId,
        @QueryParam("executionId") String executionId,
        @QueryParam("processDefinitionId") String processDefinitionId,
        @QueryParam("withRetriesLeft") Boolean withRetriesLeft,
        @QueryParam("executable") Boolean executable,
        @QueryParam("withException") Boolean withException,
        @QueryParam("dueBefore") LocalDate dueBefore,
        @QueryParam("dueAfter") LocalDate dueAfter,
        @QueryParam("exceptionMessage") String exceptionMessage,
        @QueryParam("withoutTenantId") Boolean withoutTenatId,
        @QueryParam("sort") String sort,
        @QueryParam("order") SortOrder sortOrder,
        @QueryParam("start") Integer pageStart,
        @QueryParam("size") Integer pageSize);

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public ResultList<Job> queryJobs(
        @QueryParam("id") String jobId,
        @QueryParam("processInstanceId") String processInstanceId,
        @QueryParam("executionId") String executionId,
        @QueryParam("processDefinitionId") String processDefinitionId,
        @QueryParam("withRetriesLeft") Boolean withRetriesLeft,
        @QueryParam("executable") Boolean executable,
        @QueryParam("withException") Boolean withException,
        @QueryParam("dueBefore") LocalDate dueBefore,
        @QueryParam("dueAfter") LocalDate dueAfter,
        @QueryParam("exceptionMessage") String exceptionMessage,
        @QueryParam("tenantId") String tenantId,
        @QueryParam("tenantIdLike") String tenantIdLike,
        @QueryParam("sort") String sort,
        @QueryParam("order") SortOrder sortOrder,
        @QueryParam("start") Integer pageStart,
        @QueryParam("size") Integer pageSize);

}
