/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.activiti.api;

import java.io.File;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.inteligr8.activiti.model.ProcessInstanceAction;
import com.inteligr8.activiti.model.ResultList;
import com.inteligr8.activiti.model.SortOrder;
import com.inteligr8.activiti.model.Variable;
import com.inteligr8.alfresco.activiti.model.ProcessInstance;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

@Path("/api/runtime/process-instances")
public interface ProcessInstanceApi {
    
    public enum Sort {
        @JsonProperty("id")
        ProcessInstanceId,
        @JsonProperty("processDefinitionId")
        ProcessDefinitionId,
        @JsonProperty("tenantId")
        TenantId,
        @JsonProperty("processDefinitionKey")
        ProcessDefinitionKey;
    }

    @GET
    @Path("{processInstanceId}")
    @Produces({ MediaType.APPLICATION_JSON })
    ProcessInstance get(
            @PathParam("processInstanceId") String processInstanceId);

    @DELETE
    @Path("{processInstanceId}")
    void delete(
            @PathParam("processInstanceId") String processInstanceId);

    @PUT
    @Path("{processInstanceId}")
    @Consumes({ MediaType.APPLICATION_JSON })
    void act(
            @PathParam("processInstanceId") String processInstanceId,
            ProcessInstanceAction action);

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public ResultList<ProcessInstance> getByTenant(
            @QueryParam("id") String processInstanceId,
            @QueryParam("processDefinitionKey") String processDefinitionKey,
            @QueryParam("processDefinitionId") String processDefinitionId,
            @QueryParam("businessKey") String businessKey,
            @QueryParam("involvedUser") String involvedUser,
            @QueryParam("suspended") Boolean suspended,
            @QueryParam("superProcessInstanceId") String superProcessInstanceId,
            @QueryParam("subProcessInstanceId") String subProcessInstanceId,
            @QueryParam("excludeSubprocesses") Boolean excludeSubprocesses,
            @QueryParam("includeProcessVariables") Boolean includeProcessVariables,
            @QueryParam("tenantId") String tenantId,
            @QueryParam("sort") Sort sort,
            @QueryParam("order") SortOrder sortOrder,
            @QueryParam("start") Integer pageStart,
            @QueryParam("size") Integer pageSize);

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public ResultList<ProcessInstance> getByTenants(
            @QueryParam("id") String processInstanceId,
            @QueryParam("processDefinitionKey") String processDefinitionKey,
            @QueryParam("processDefinitionId") String processDefinitionId,
            @QueryParam("businessKey") String businessKey,
            @QueryParam("involvedUser") String involvedUser,
            @QueryParam("suspended") Boolean suspended,
            @QueryParam("superProcessInstanceId") String superProcessInstanceId,
            @QueryParam("subProcessInstanceId") String subProcessInstanceId,
            @QueryParam("excludeSubprocesses") Boolean excludeSubprocesses,
            @QueryParam("includeProcessVariables") Boolean includeProcessVariables,
            @QueryParam("tenantIdLike") String tenantIdLike,
            @QueryParam("sort") Sort sort,
            @QueryParam("order") SortOrder sortOrder,
            @QueryParam("start") Integer pageStart,
            @QueryParam("size") Integer pageSize);

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public ResultList<ProcessInstance> getByAny(
            @QueryParam("id") String processInstanceId,
            @QueryParam("processDefinitionKey") String processDefinitionKey,
            @QueryParam("processDefinitionId") String processDefinitionId,
            @QueryParam("businessKey") String businessKey,
            @QueryParam("involvedUser") String involvedUser,
            @QueryParam("suspended") Boolean suspended,
            @QueryParam("superProcessInstanceId") String superProcessInstanceId,
            @QueryParam("subProcessInstanceId") String subProcessInstanceId,
            @QueryParam("excludeSubprocesses") Boolean excludeSubprocesses,
            @QueryParam("includeProcessVariables") Boolean includeProcessVariables,
            @QueryParam("tenantId") String tenantId,
            @QueryParam("tenantIdLike") String tenantIdLike,
            @QueryParam("withoutTenantId") Boolean withoutTenantId,
            @QueryParam("sort") Sort sort,
            @QueryParam("order") SortOrder sortOrder,
            @QueryParam("start") Integer pageStart,
            @QueryParam("size") Integer pageSize);

    @GET
    @Path("{processInstanceId}/diagram")
    @Produces({ "image/png" })
    File getDiagram(
            @PathParam("processInstanceId") String processInstanceId);

    @GET
    @Path("{processInstanceId}/variables")
    @Produces({ MediaType.APPLICATION_JSON })
    List<Variable> getVariables(
            @PathParam("processInstanceId") String processInstanceId);

    @GET
    @Path("{processInstanceId}/variables/{variableName}")
    @Produces({ MediaType.APPLICATION_JSON })
    Variable getVariable(
            @PathParam("processInstanceId") String processInstanceId,
            @PathParam("variableName") String variableName);

    @POST
    @Path("{processInstanceId}/variables")
    @Consumes({ MediaType.APPLICATION_JSON })
    void createVariables(
            @PathParam("processInstanceId") String processInstanceId,
            List<Variable> variables);

    @PUT
    @Path("{processInstanceId}/variables")
    @Consumes({ MediaType.APPLICATION_JSON })
    void updateVariables(
            @PathParam("processInstanceId") String processInstanceId,
            List<Variable> variables);
    
    @PUT
    @Path("{processInstanceId}/variables/{variableName}")
    @Consumes({ MediaType.APPLICATION_JSON })
    void updateVariable(
            @PathParam("processInstanceId") String processInstanceId,
            @PathParam("variableName") String variableName,
            Variable variable); 

}
