/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.activiti.api;

import com.inteligr8.activiti.api.ProcessInstanceApi.Sort;
import com.inteligr8.activiti.model.ProcessDefinition;
import com.inteligr8.activiti.model.ProcessDefinitionAction;
import com.inteligr8.activiti.model.ResultList;
import com.inteligr8.activiti.model.SortOrder;

import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

@Path("/api/repository/process-definitions")
public interface ProcessDefinitionApi {

    @GET
    @Path("{processDefinitionId}")
    @Produces({ MediaType.APPLICATION_JSON })
    ProcessDefinition get(
            @PathParam("processDefinitionId") String processDefinitionId);

    @PUT
    @Path("{processDefinitionId}")
    @Consumes({ MediaType.APPLICATION_JSON })
    void act(
            @PathParam("processDefinitionId") String processDefinitionId,
            ProcessDefinitionAction action);

    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    public ResultList<ProcessDefinition> getByAny(
            @QueryParam("category") String category,
            @QueryParam("categoryLike") String categoryLike,
            @QueryParam("categoryNotEquals") String categoryNotEquals,
            @QueryParam("key") String key,
            @QueryParam("keyLike") String keyLike,
            @QueryParam("name") String name,
            @QueryParam("nameLike") String nameLike,
            @QueryParam("resourceName") String resourceName,
            @QueryParam("resourceNameLike") String resourceNameLike,
            @QueryParam("version") String version,
            @QueryParam("suspended") Boolean suspended,
            @QueryParam("latest") Boolean latest,
            @QueryParam("deploymentId") String deploymentId,
            @QueryParam("startableByUser") String startableByUser,
            @QueryParam("tenantId") String tenantId,
            @QueryParam("tenantIdLike") String tenantIdLike,
            @QueryParam("sort") Sort sort,
            @QueryParam("order") SortOrder sortOrder,
            @QueryParam("start") Integer pageStart,
            @QueryParam("size") Integer pageSize);

}
