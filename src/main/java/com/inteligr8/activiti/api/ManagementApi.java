/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.activiti.api;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import com.inteligr8.activiti.model.Engine;
import com.inteligr8.activiti.model.EngineProperties;

@Path("/api/management")
public interface ManagementApi {

    @GET
    @Path("engine")
    @Produces({ MediaType.APPLICATION_JSON })
    public Engine getEngine();

    @GET
    @Path("properties")
    @Produces({ MediaType.APPLICATION_JSON })
    public EngineProperties getEngineProperties();

}
